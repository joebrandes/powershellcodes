﻿# Beispielskript mit neuen Teilnehmern (OU und Gruppe)
# alle gleiches Kennwort 
# WPS2_ADS_UserCreate_Schulungsteilnehmer.ps1

# Massenanlegen von Schulungsteilnehmern
# (C) Holger Schwichtenberg, www.IT-Visions.de, 2012-2014
# ------------------------------------------------------
clear


$AnzahlBenutzer = 3
$wurzelou = "ou=Teilnehmer,dc=firma,dc=local"
$Kennwort = "P@ssw0rd"
$gruppenname = "Teilnehmer"
$ErrorActionPreference = "continue"

import-module activedirectory
cd ad:

# ------------ OU löschen und wieder anlegen
# -------------------------------------------------------
Function ReNew-OU([string] $oupfad)
{
if ((Test-Path "AD:\$oupfad" -WarningAction silentlycontinue))
{
 "OU wird gelöscht: " + $oupfad
 Set-ADObject $oupfad -ProtectedFromAccidentalDeletion $false
 Remove-ADObject $oupfad -confirm:$false -Recursive
 }
"OU wird angelegt: " + $oupfad
$parent = split-path $oupfad 
$ouname = (split-path $oupfad -Leaf).Replace("ou=","")
$ou = New-ADOrganizationalUnit  -name $ouname -path $parent -PassThru
"OU ist angelegt: " + $ou.distinguishedname
}

# ------------ Gruppe erzeugen, wenn nicht vorhanden
# -------------------------------------------------------
Function NewIfNotExists-Group($wurzelpfad, $name){
$pfad = join-path  $wurzelpfad "cn=$name"
if (-not (Test-Path "AD:/$pfad"))
    {
    $g = New-ADGroup -path $wurzelpfad -Name $name -SamAccountName  $name -GroupScope Global -GroupCategory Security -Description  $name –PassThru
    "Gruppe angelegt " + $g.DistinguishedName
    }
}

# ------------ Benutzer einer Gruppe hinzufügen, Gruppe ggf. anlegen
# -------------------------------------------------------
function Add-ADGroupMemberEx($wurzelpfad, $gruppename, $mitglied)
{
NewIfNotExists-Group $wurzelpfad $gruppename
"--- Mitglieder in die Gruppe aufnehmen..."
Add-ADGroupMember -Identity $gruppename -Members $mitglied
"Benutzer $mitglied in $gruppename aufgenommen"
}


# ********************* Hauptroutine

"Erstellen der Wurzel-OU"
ReNew-OU $wurzelou

"Anlegen der Benutzer..."

# Hauptschleife für n Elemente
for($i = 1; $i -le $AnzahlBenutzer;$i++) {

$unteroupath = $wurzelou

$vorname = "Vorname #$i"
$Nachname = "Nachname #$i"
# Eigenschaften des Benutzers
$verzeichnisname = "Teilnehmer$i"
$Anzeigename = "Teilnehmer #$i"
$SamAccountName = "Teilnehmer$i"
$kennwort = $Kennwort
$title = ""


# Benutzer anlegen
$benutzerObj = New-ADUser -GivenName $vorname -Surname $Nachname -path $unteroupath –Name $verzeichnisname –SamAccountName $SamAccountName –DisplayName $Anzeigename –Title $title –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString $Kennwort -AsPlainText -force) -PassThru

if ($benutzerObj -ne $null){
    "Benutzer #" +$i + ":" + $Anzeigename + " angelegt: SID=" + $benutzerObj.Sid + " Kennwort=" + $kennwort

    # Gruppe hinzufügen, ggf. Gruppe anlegen
    Add-ADGroupMemberEx  $wurzelou $gruppenname $SamAccountName
	# $ADDomSID = (Get-ADDomain).DomainSID.Value # S-1-5-21-...
    Add-ADGroupMember -Identity "Domain admins" -Members $SamAccountName
	# oder besser:
	# Add-ADGroupMember -Identity "$ADDomSID-512" -Members $SamAccountName
    }
}

"Skript ist fertig!"

<#
# ==========
# Skriptende
# ==========
#>
