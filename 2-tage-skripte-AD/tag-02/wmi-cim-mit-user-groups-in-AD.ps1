﻿# WMI/CIM-Klassen für AD
# ======================
# zu beachten: Get-WmiObject für Remote/WinRM
# Get-CimInstance kennt kein -credential
# also z.B. Invoke-Command benutzen
# Anm.: Möglichkeiten sehr eingeschränkt!


# Alle Konten
Get-CimInstance Win32_Account
# bzw.
Invoke-Command -ComputerName domvbox2012r2 -Credential $cred -SkriptBlock { ... }
# Nur die Benutzerkonten erreicht man mit:
Get-CimInstance Win32_UserAccount
# Nur die Gruppen erreicht man mit:
Get-CimInstance Win32_Group
# gezielt Objekte herausfiltern:
# Name und Domäne der Benutzerkonten, deren Kennwort niemals verfällt
Get-CimInstance Win32_useraccount | Where-Object {$_.passwordexpires -eq 0 } | Select-Object Name, Domain
# alternativ:
Get-CimInstance Win32_Useraccount -filter “passwordexpired=’false’” | Select-Object Name, Domain
# ob Benutzer „FBI\FoxMulder“ einen Bildschirmschoner auf dem Computer „AgentPC04“ aktiviert hat.
Get-CimInstance Win32_Desktop -computer AgentPC04 | Where-Object { $_.Name -eq “DBI\FoxMulder” } | Select-Object screensaveractive

#Get-WmiObject win32_group
Get-wmiobject -Class ds_group -Namespace root\directory\ldap  -Filter "DS_name like 'm%'"

