﻿
# Der folgende Befehl liefert aus dem aktuellen Active-Directory-Pfad die ersten fünf Einträge, deren Name mit „F“ beginnt.
Get-ADObject -Filter 'Name -like "f*"' -SearchScope SubTree -resultSetsize 5

# Der folgende Befehl liefert alle Einträge, was durch $null bei resultSetsize anzugeben ist
# (im Standard würden sonst nur die ersten 1000 ausgegeben!).
Get-ADObject -Filter 'Name -like "f*"' -SearchScope SubTree -resultSetsize $null

# Der folgende Befehl listet alle Einträge auf, in denen der Vorname mit „F“ und der Nachname mit „M“ beginnt.
# ACHTUNG: Man muss die LDAP-Attributnamen (z.?B. "sn") verwenden, nicht die Attributnamen der PowerShell (wie "surname").

Get-ADObject -Filter 'givenname -like "f*" -and sn -like "m*"' -SearchScope SubTree -resultSetsize $null


# Anstelle der moduleigenen Filtersyntax kann man auch die LDAP-Suchsprache verwenden. 
# Das folgende Beispiel sucht korrekt in allen Benutzerkonten, die mit „F“ beginnen.

Get-ADObject -LDAPFilter '(&(objectCategory=person)(objectClass=user)(name=f*))' -SearchScope SubTree -resultSetsize $null 
# Die gleichzeitige Verwendung von objectCategory und objectClass in der Suchanfrage steigert die Leistung.
# Vergleichsoperatoren  in
# https://blogs.msdn.microsoft.com/adpowershell/2009/04/14/active-directory-powershell-advanced-filter-part-ii/ 


# PowerShell Syntax deutlich pflegeleichter als die LDAP-Syntax
$date = (Get-date) - (New-Timespan -days 5)
Get-ADUser -Filter { lastLogon -gt $date }
# Mit der LDAP-Syntax wäre dies (die Zeitangabe erfolgt in Einheiten zu 100 Nanosekunden seit dem 1.1.1601):
Get-ADUser -LDAPFilter "(&(lastLogon>=128812906535515110) (objectClass=user) (!(objectClass=computer)))"