﻿<#
# WMI / CIM
# ================
#>


# Vergleich WMI vs. CIM:
# https://blogs.technet.microsoft.com/heyscriptingguy/2016/02/08/should-i-use-cim-or-wmi-with-windows-powershell/ 
# Introduction to CIM-Cmdlets:
# https://blogs.msdn.microsoft.com/powershell/2012/08/24/introduction-to-cim-cmdlets/ 


# search for WMI / CIM class name
Get-WmiObject -Class *Share* -List
Get-CimClass -ClassName *Share*
# search for WMI / CIM class name
Get-WmiObject -Class Win32_Share
(Get-CimClass -ClassName Win32_Share).CimClassMethods

# auch mit Select-String
Get-WmiObject -List | Select-String Print

# sinnvolle Class rund um Disk mit mehr als 5 Properties und nichts mit Perf
$Keyword = ‘disk’
Get-WmiObject -Class “Win32_*$Keyword*” -List |
Where-Object { $_.Properties.Count -gt 5 -and $_.Name -notlike ‘*_Perf*’ }
# tatsächliche Ergebnisse dann mit 
Get-WmiObject -Class Win32_DiskPartition

# besonders nützliche Classes
Select-Xml $env:windir\System32\WindowsPowerShell\v1.0\types.ps1xml -XPath /Types/Type/Name |
    ForEach-Object { $_.Node.InnerXML } | Where-Object { $_ -like ‘*#root*’ } |
    ForEach-Object { $_.Split(‘[\/]’)[-1] } | Sort-Object -Unique

# einfach mal eine rauspicken
Get-WmiObject -Class Win32_BIOS


# WMI Online-Hilfen per Funktion finden
# =====================================
function Get-WmiHelpLocation
{
param ($WmiClassName='Win32_BIOS')
$Connected = [Activator]::CreateInstance([Type]::GetTypeFromCLSID([Guid]'{DCB00C01-570F-4A9B-8D69-199FDBA5723B}')).IsConnectedToInternet
if ($Connected)
{
$uri = ‘http://www.bing.com/search?q={0}+site:msdn.microsoft.com’ -f $WmiClassName
# in  altem Skriptbeispiel stand bei Where-Object href -like noch http - dann $uri leer!
$url = (Invoke-WebRequest -Uri $uri -UseBasicParsing).Links | Where-Object href -like 'https://msdn.microsoft.com*' | Select-Object -ExpandProperty href -First 1
Start-Process $url
$url
}
else
{
Write-Warning ‘No Internet Connection Available.’
}
}


# License Status
Get-WmiObject SoftwareLicensingService

# Alle Namespaces auflisten
Get-WmiObject -Query “Select * from __Namespace” -Namespace Root | Select-Object -ExpandProperty Name
# Infos zu bestimmten Namespace
Get-WmiObject -Namespace root\SecurityCenter2 -List
Get-WmiObject -Namespace root\SecurityCenter2 -Class AntivirusProduct


# aktuellen Energiemodus evaluieren
$PowerPlan = (Get-WmiObject -Namespace root\cimv2\power -Class Win32_PowerPlan -Filter ‘isActive=True’).ElementName
“Current power plan: $PowerPlan”

# Service Start Modi - Anmerkung: Get-Service kann keinen StartMode!
Get-WmiObject Win32_Service | Select-Object Name, StartMode
# ein spezieller Startmodus
([wmi]'Win32_Service.Name="Spooler"').StartMode


# eingeloggte User
$ComputerName = ‘localhost’
Get-WmiObject Win32_ComputerSystem -ComputerName $ComputerName | Select-Object -ExpandProperty UserName


# Netzwerkanalyse
function Get-NetworkConfig {
Get-WmiObject Win32_NetworkAdapter -Filter ‘NetConnectionStatus=2’ |
ForEach-Object {
    $result = 1 | Select-Object Name, IP, MAC
    $result.Name = $_.Name
    $result.MAC = $_.MacAddress
    $config = $_.GetRelated(‘Win32_NetworkAdapterConfiguration’)
    $result.IP = $config | Select-Object -ExpandProperty IPAddress
    $result
    }
}
# Funktion aufrufen:
Get-NetworkConfig

# Lokale Gruppen
Get-WmiObject Win32_Group -Filter "domain='$env:computername'" | Select-Object Name,SID


# Uptime OS
$os = Get-WmiObject -Class Win32_OperatingSystem
$boottime = [System.Management.ManagementDateTimeConverter]::ToDateTime($os.LastBootupTime)
$timedifference = New-TimeSpan -Start $boottime
$days = $timedifference.TotalDays
'Das System läuft seit {0:0.000} Tagen.' -f $days

# Freier Speicher auf Disks
Get-WmiObject Win32_LogicalDisk | 
    ForEach-Object { ‘Disk {0} hat {1,20:0.00} MB Platz frei’ -f $_.Caption, ($_.FreeSpace / 1MB)
}


# ================================================================
# weitere Beispiele bei Weltner - Best  Practises wmi_cookbook.pdf
# ================================================================

