﻿# Ausdrücke
# =========

10* (8 + 6)
"Hello " + "    Palimpalim     " + "World"
# Command Mode
Write-Output 10 * (8 + 6)
# vergleichen mit
Write-Output (10 * (8 + 6))

"Anzahl der laufenden Prozesse: (Get-Process).Count"
# vergleichen mit: Subexpression mit $
# bei einfachen Anführungszeichen wird nie berechnet!
'Anzahl der laufenden Prozesse: $((Get-Process).Count)'   
# jetzt mit Berechnungen innerhalb/außerhalb von Strings
"Anzahl der laufenden Prozesse: $((Get-Process).Count)"
"Anzahl der laufenden Prozesse: " + (Get-Process).Count