# Spezial Cmdlet der PowerShell für selbssignierte Zertifikate nutzen
New-SelfSignedCertificate -CertStoreLocation "CERT:\Currentuser\MY" -Subject "CN=MachDeinDing" -TextExtension "2.5.29.37={text}1.3.6.1.5.5.7.3.3"

# Zertifikatsspeicher Anzeigen lassen
# siehe auch certmgr.msc als Gui-Vergleich
dir cert:/currentuser/my 

# Zertifikat auswählen (Index beachten, falls mehrere verfügbar)
$cert = @(dir "cert:/currentuser/my/")[0] 

# Skript signieren - gerne auch als Schleife
Set-AuthenticodeSignature YourScript.ps1 $cert        