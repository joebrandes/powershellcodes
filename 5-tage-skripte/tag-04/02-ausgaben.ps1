﻿<#
# Ausgaben
# ========
#>

# Cmdlets mit Out-* und Format-*

Get-Service | Select-Object -First 5 | Format-Table
Get-Service | Select-Object -First 5 | Format-Table *
Get-Service | Select-Object -First 5 | Format-Table -Property Name, CanStop

Get-Service | Select-Object * | Out-GridView

# Standardausgabe gemäß DotNetTypes.Format.ps1xml (eigentlich also Dot.NET Formate!)
Get-ViewDefinition System.Diagnostics.Process      # Get-ViewDefinition benötigt PSCX Modul
Get-Process | Format-Table -View priority
# Ausgaben einschränken
Get-Process | Format-Table -Property id,processname,workingset 
# als auch
Get-Process | Select-Object Id, ProcessName, WorkingSet | Format-Table 

# Seitenweise Ausgabe
Get-Service | Out-Host -Paging                     # kein Paging im ISE
# das alte more.exe ginge natürlich auch (aber hier asynchrone Abarbeitung - Nerv!)
Get-ChildItem c:\ -Recurse | more

# Mit Write-Host manuelle Konfiguration möglich
Write-Host "Hallo Joe" -foregroundcolor red -backgroundcolor white

# Spezialzeichen - Escape-Sequenzen mit Gravis/Backticks
# `n  Zeilenumbruch
# `a  Sound - kein Sound in ISE oder VSCode
$text = "Zeile 1`nZeile 2`a "

$a = "Joe Brandes"
$b = "seminar@firma.de"
$c = Get-Date
# und wieder: in Zeichenketten (doppelte) sind Variablen nutzbar; ansonsten immer +
$a + " ist erreichbar unter " + $b + ". Diese Information hat den Stand: " + $c + "."
# oder:
"$a ist erreichbar unter $b. Diese Information hat den Stand: $c."

# mit Platzhaltern und Formatbezeichnern: Ausgabeoperator –f
# Beispiel ab S. 182ff Schwichtenberg
"{0} ist erreichbar unter {1}. Diese Information hat den Stand: {2:D}." -f $a, $b, $c
# Beispiele
Get-Process | ForEach-Object { "{0,-40} - {1}" -f $_.Name, ($_.WS/1MB) }
Get-Process | ForEach-Object { "{0,-40} - {1:n}" -f $_.Name, ($_.WS/1MB) }
Get-Process | ForEach-Object { "{0,-40} mit {1:0.000} MB" -f $_.Name, ($_.WS/1MB) }


# Benutzerdefinierte Ausgabeformatierung
# ======================================
# Beispiel mit drei Spalten:
# 5 Zeichen für die Prozessnummer in der ersten Spalte
# 20 Zeichen für den Prozessnamen
# 11 Zeichen für die Speichernutzung, 
# wobei die letzte Angabe in Megabyte erfolgt und mit einer Nachkommastelle
Get-Process | Sort-Object workingset64 -Descending | 
	Format-Table @{Label="Nr"; Expression={$_.ID}; Width=5}, 
				@{Label="Name"; Expression={$_.Processname}; Width=20 }, 
				@{Label="Speicher MB"; Expression={$_.WorkingSet64 / 1MB}; Width=11; Format="{0:0.0}" }



# Unterobjekt mit eigenen Methoden und Eigenschaften richtig ausgeben:
Get-Process | Format-Table ProcessName, { $_.TotalProcessorTime.Milliseconds }
Get-Process | Sort-Object cpu | Format-Table ProcessName, { $_.TotalProcessorTime.MilliSeconds }
