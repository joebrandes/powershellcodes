﻿
# System.DirectoryServices.ActiveDirectory
# alias Active Directory Management Objects – ADMO
# ===================

# Listing 54.32 
# Informationen über die Domäne und den Forest 
# [ADS_Domain_Info.ps1]

# Aktuelle Domain ermitteln
$d = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain();

# Informationen über aktuelle Domäne
"Name: " + $d.Name
"Domain Mode: " + $d.DomainMode
"Inhaber der InfrastructureRole: " + $d.InfrastructureRoleOwner.Name
"Inhaber der PdcRole: " + $d.PdcRoleOwner.Name
"Inhaber der PdcRole: " + $d.PdcRoleOwner.Name

# Informationen über Forest der aktuellen Domäne
$f = $d.Forest;
"Name der Gesamtstruktur: " + $f.Name
"Modus der Gesamtstruktur: " + $f.ForestMode



# Listing 54.33 
# Informationen über die Domänencontroller und ihre Rollen  
# [ADS_Domaincontroller_Info.ps1]

# Display current domain
$d = [System.Directoryservices.ActiveDirectory.Domain]::GetCurrentDomain()
$DCs = $d.DomainControllers
# Loop over all domain controllers 
foreach ($DC in $DCs)
{
   "Name: " + $DC.Name
   "IP: " + $DC.IPAddress.ToString()
   "Time: " + $DC.CurrentTime.ToString()
    "Roles:"
   # Loop over all roles of DC
    foreach ($R in $DC.Roles)
    {
     "- " + $R.ToString()
    }
}

