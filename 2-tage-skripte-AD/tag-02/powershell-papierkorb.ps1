﻿# PowerShell Active Directory Papierkorb
# ======================================

# https://www.der-windows-papst.de/2016/03/01/active-directory-papierkorb-powershell-befehle/ 
# https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/dd379481%28v%3dws.10%29 
# https://docs.microsoft.com/de-de/windows-server/identity/ad-ds/get-started/adac/advanced-ad-ds-management-using-active-directory-administrative-center--level-200- 

# über DSAC (Administrative Center) 
# oder natürlich über die PowerShell aktivieren:

Enable-ADOptionalFeature –Identity "CN=Recycle Bin Feature,CN=Optional Features,CN=Directory Service,CN=Windows NT,CN=Services,CN=Configuration,DC=dom-2016,DC=local" –Scope ForestOrConfigurationSet –Target dom-2016.local

# Überprüfen ob User Test1 vorhanden ist
Get-ADUser -Identity Test1

# User Test1 markieren und löschen
Get-ADUser -Identity Test1 | Remove-ADUser -Confirm:$false

# Überprüfen ob User Test1 nach dem Löschen vorhanden ist
Get-ADUser -Identity Test1

# User Test1 im Papierkorb suchen
Get-ADObject -Filter {Name -like "Test1*"} -IncludeDeletedObjects

# User Test1 wiederherstellen
Get-ADObject -Filter {displayName -eq "Test1"} -IncludeDeletedObjects | Restore-ADObject

Get-ADObject -Filter {displayName -eq "Test1"} -IncludeDeletedObjects | Restore-ADObject -TargetPath "OU=Users,OU=Konfiguration,DC=NDSEDV,DC=DE"

# User Test1 wiederherstellen
Restore-ADObject –identity cf0856c4-1563-4d8b-90c0-46066f1fe1b5

# User Details auslesen
Get-ADObject -Filter {samaccountname -eq "Test1"} -IncludeDeletedObjects -Properties *

# User Details anzeigen lassen GUID ObjectClass OU
Get-ADObject -Filter {Name -like "Test1"}

# Details aller User anzeigen lassen GUID ObjectClass OU
Get-ADObject -filter {ObjectClass -like "user"}

Get-ADObject -filter {ObjectClass -like "user"} -IncludeDeletedObjects | ogv

# User Test1 suchen und in den Ursprungspfad wiederherstellen
Get-ADObject -filter 'samaccountname -eq "Test1"' -IncludeDeletedObjects -properties * | Foreach-Object {Restore-ADObject $_.objectguid -NewName $_.samaccountname -TargetPath $_.LastKnownParent}


