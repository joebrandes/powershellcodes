﻿# einfaches Löschen der erstellten Objekte:
# -----------------------------------------
# WICHTIG: Rückfrage zum Löschen einbauen!!

# zum Löschen der angelegten OUs / OU-Hierarchien muss
# erst der Schutz vor versehentlichem Löschen entfernt werden
# Tipp: die Aufrufe brauchen mindestens did Mandatory Parameter in Zeile
# hier: -Identity
# Zeilenumbrüche dann - wie gewohnt - mit Backticks `
Set-ADObject -Identity:"OU=Testing,DC=dom2012r2,DC=local" `
	-ProtectedFromAccidentalDeletion:$false `
	-Server:"domvbox-2012r2.dom2012r2.local"

# Danach kann -Recursive (nicht -Recurse) gelöscht werden!
Remove-ADObject -Confirm:$false -Identity:"OU=Testing,DC=dom2012r2,DC=local" `
	-Recursive:$true `
	-Server:"domvbox-2012r2.dom2012r2.local"

