﻿# Desktop Ordner finden
[System.Environment]::GetFolderPath('Desktop')
# Folder anzeigen für GetFolderPath:
[System.Enum]::GetNames([System.Environment+SpecialFolder])
# also:
[System.Environment]::GetFolderPath('Cookies')


# Dateien umbenennen - hier: die Win8/10 Screenshots haben Leerzeichen!
# ==================
$global:i = 100
# $path = "$env:userprofile\Pictures\Screenshots"
$path = "C:\powershell-bu\daten"
Get-ChildItem -Path $path -Filter *.png | 
	Rename-Item -NewName { "screenshot_$i.png"; $global:i++ }