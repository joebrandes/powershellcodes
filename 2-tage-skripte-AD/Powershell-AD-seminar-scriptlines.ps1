﻿<#
     _            ____    ____                 _
    | | ___   ___| __ )  / ___|  ___ _ __ ___ (_)_ __   __ _ _ __ ___
 _  | |/ _ \ / _ \  _ \  \___ \ / _ \ '_ ` _ \| | '_ \ / _` | '__/ _ \
| |_| | (_) |  __/ |_) |  ___) |  __/ | | | | | | | | | (_| | | |  __/
 \___/ \___/ \___|____/  |____/ \___|_| |_| |_|_|_| |_|\__,_|_|  \___|


https://www.belnet.de/schulung-und-zertifizierung/seminare/seminar/w-1101-powershell-aufbauworkshop-active-directory/#cat:218

Schulungsinhalt

    Cmdlets für die Active Directory Verwaltung
    Besonderheiten der AD Cmdlets (Aufrufe, Parameter)
    Benutzer, Gruppen und OUs mit der PowerShell
    Arbeiten mit CSV Dateien und Exports aus AD
    Automatisierungen und Skripting im AD
    Gruppenrichtlinienverwaltung
    Aktivieren und Nutzen des AD Papierkorbs
    Auswertung und Filtern von Logdateien
    Deployment von AD-Strukturen und DCs
    PowerShell für AD-Verwaltungen auf Mitgliedsservern und Clients
    Tipps und Tricks aus der Praxis

#>

<#
Seminar-Faden - Orientierung für 2-Tages-Seminar 
================================================

Intro - TN-Themen / Kenntnisse

Übersicht Hyper-V-Domäne (Maschinen / Netzwerk)

powershell.joe-brandes.de (PS Grundlagen Seminarskript online)
	s.a. PowerShell Cookbook Chapter 26 Active Directory 

HistorySavePath (Get-PSReadline)
	und PS Transcripts: (s.a. Cmdlet *transcript*)
	GPO (Computer oder Benutzer) - Administrative Templates – Windows Components –  Windows PowerShell and double-klick "Turn on PowerShell Transcription"

Erste PowerShell Starts: Get-Module, ...

Modul ActiveDirectory - Get-Command -Module ..., 

Hilfe zu Get-ADUser ... (Get-Help benötigt Online-Verbindung)
	Script: 40-aduser-intro.ps1

Get-ADDomain, Get-ADDomainController

Get-ADObject: (s.a. Get-Member) 
	aus Get-ADObject * wird: (Get-ADObjekt -Filter "*").count 
	Get-ADObject '...' -Properties *
	Script: 10-adobject-handling.ps1
	Benutzerkonto aus Stadt Braunschweig: ADSI-Editor
		Get-ADObject -Filter 'l -like "b*"' -Properties * 
	
	Analyse der Benutzer mit ADSI-Editor und ldp (Verbinden -> Binden -> Struktur)
		Get-ADObject -LDAPFilter  wie z.B.:
		Get-ADObject -LDAPFilter "(objectClass=site)" -SearchBase 'CN=Configuration,DC=Fabrikam,DC=Com' -Properties CanonicalName | FT Name,CanonicalName -A
		Get-ADObject -LDAPFilter '(&(objectCategory=person)(objectClass=User)(name=T*))'
		Tipp: komplizierte LDAP-Action mit DSA.msc -> Gespeicherte Abfragen...

PSDrive: AD - in AD bewegen - sl 'DC=firma,DC=local' ...

ADAC nutzen: neue OU erstellen - PS History ...

New-PSDrive für ADs neue OU erstellen:
	für $PROFILE ggf. Import-Module hinzufügen
	# Pfad setzen:
	Set-Location AD:\"ou=Testing,dc=firma,dc=local"
	# und natürlich lassen sich PSDrives erstellen im AD:
	New-PSDrive -Name Testing -PSProvider ActiveDirectory -Root AD:\"ou=Testing,dc=firma,dc=local"

Neuen AD-User per PS anlegen: 
	Script: 50-aduser-benutzer-anlegen-per-skript-und-co.ps1 

(Anm.: old stuff - Windows 7 Zeiten - für Windows 10 nicht nötig)
Photo für User hinterlegen: "Spielerei"
	   https://znil.net/index.php?title=Foto_im_Active_Directory_hinterlegen_und_bei_Benutzeranmeldung_anzeigen
Neuere Lösung: 
       https://jocha.se/blog/tech/ad-user-pictures-in-windows-10


Logs mit Get-EventLog

... weiter geht es ...

Forts. Benutzer anlegen - Step by Step - dann in Massen
	powershell_create_bulk_users
	ou-und-massen-accounts-schwichtenberg
	
Gruppenrichtlinien

AD Deployment

Papierkorb

Hintergrundjobs 

PowerShell auf Client (RSAT)

Remoting

Online-AD-Beispiele für
   AD Verwaltungen mit PowerShell
   ==============================   
   ab Zeile 1777 - eine "kleine" Sammlung
   ==============================



#>



<#
Roter Faden AD
==============

PowerShell Modul "Active Directory" (ADPowerShell)

TIPP: Zu beachten ist, dass zur Navigation im Active Directory der Provider
nicht den Namen, sondern den DN (Distinguished Name) verwendet. Falsch ist
also:

Dir ad:\FBI

(auch wenn die Anzeige von DIR ad: dies suggeriert), sondern richtig ist

Dir ad:\"dc=FBI,dc=org"     (mit den Anführungszeichen!).


... alles Weitere in den Snippets / Scriptlines Sammlungen und hier...
Kapitelbezeichnung (in Scriptlines)
Modul ActiveDirectory (einfach mit Strg + F suchen; oder Strg + G ca. Zeile 2000)


PowerShell in Büchern:
======================

PowerShell 5.0 - Das Praxisbuch - Schwichtenberg, Holger
Kapitel 47 ab S. 737ff
PowerShell 5 und Core 6 - Praxisbuch
Kapitel 54 ab S. 867ff

PowerShell Cookbook (bis inkl. PS 3.0)
Chapter 26 ab S. 669 
Anm.: alles mit [ADSI] !

Gruppenrichtlinien - Voges, Holger & Dausch, Martin
Kapitel 17 ab S. 415ff

Spezial PDF "PowerShell Grundlagen und AD" - Voges, netz-weise-it
ab S. 32-39
ab S. 40 LDAP-Filter verstehen

es folgt:
Scriptlines PowerShell (AD)
===========================
#>






# Modul ADPowerShell für das Active Directory
# Auf Server einfach über Tools: AD-Modul für PowerShell
# oder einfach
Get-Help Get-ADUser
# Auf Clients bitte passendes RSAT installieren
# kleines Problem zum Zeitpunkt 06/2018: 3 verschiede 64-Bit-Varianten!
# https://www.windowspro.de/news/rsat-fuer-windows-10-1803-april-2018-update-verfuegbar/03965.html 
# 
# Versionen: RSAT_WS_1803 oder RSAT_WS_1709
# MS-Link für Downloads RSAT Windows 10
# https://www.microsoft.com/de-de/download/details.aspx?id=45520

# RSAT ab Windows 10 1809 kein eigener Download mehr
# https://www.windowspro.de/news/rsat-fuer-windows-10-kuenftig-kein-eigener-download-mehr/03985.html
Get-WindowsCapability -Online | Where-Object { Name -like *RSAT* }
# alle Tools installieren, die lokal noch nicht vorhanden sind
Get-WindowsCapability -Online |
	Where-Object {$_.Name -like "*RSAT*" -and $_.State -eq "NotPresent"} |
	Add-WindowsCapability -Online

# Auf Memberserver einfach manuell oder mittels: 
Import-Module servermanager
Add-WindowsFeature -Name "RSAT-AD-PowerShell" -IncludeAllSubFeature

# Provider bzw. Laufwerk AD:
Get-ChildItem AD:

# Modul "ActiveDirectory"; früher auch: PSCX – PowerShell Community Extensions
Get-ADObject   # 

# Erste Aufrufe für PSDrive AD: (PSProvider ActiveDirectory)
Get-ChildItem AD:

Get-ChildItem AD:\"dc=firma,dc=local"
# wir benötigen LDAP - nicht die Domänennamen wie hier firma und Anführungszeichen

# Cmdlets des Moduls - Server 2016: 147
(Get-Command -Module ActiveDirectory).Count

# Pfad setzen:
Set-Location AD:\"ou=Testing,dc=firma,dc=local"
# und natürlich lassen sich PSDrives erstellen im AD:
New-PSDrive -Name Testing -PSProvider ActiveDirectory -Root AD:\"ou=Testing,dc=firma,dc=local"
Get-ChildItem Testing:


# Allgemeine Verwaltungscmdlets
# -----------------------------
# Get-ADObject : holt ein AD-Objekt
# Set-ADObject : setzt Werte in einem AD-Objekt
# New-ADObject : erzeugt ein neues AD-Objekt (unter Angabe des Klassennamens)
# Remove-ADObject : löscht ein AD-Objekt
# Rename-ADObject : Umbenennen eines AD-Objekts
# Move-ADObject : Verschieben eines AD-Objekts
# Restore-ADObject : Wiederherstellen eines gelöschten AD-Objekts


# Beispiele:
# WPS56_Beispiele_24042017\3_Einsatzgebiete\Verzeichnisdienste\WPSModule
# also aus neuem PowerShell 5 und Core 6 Buch Schwichtenberg


# Listing 54.22 Beispielskript für den Einsatz der allgemeinen AD-Commandlets  
# [WPS2_ADS_CommonCommandlets.ps1]
# siehe 3 Optionen

"Delete and Recreate an OU....."

$ou = Get-ADObject "ou=Agents,dc=FBI,dc=org" 
$ou | fl

set-adobject "ou=Agents,dc=FBI,dc=org" -protectedFromAccidentalDeletion $false
Remove-ADObject "ou=Agents,dc=FBI,dc=org" -confirm:$false -recursive

$newou = New-ADObject -type "OrganizationalUnit" -ProtectedFromAccidentalDeletion $false -name "Alien-Agents" -Path "ou=Aliens,dc=FBI,dc=org"

"New OU:"
$newou

"Move an OU..."

Move-ADObject -Identity "ou=Alien-Agents,ou=Aliens,dc=FBI,dc=org" -targetpath "dc=FBI,dc=org"

Rename-ADObject "ou=Alien-Agents,dc=FBI,dc=org" -newname "Agents"

# Option #1
Set-ADObject "ou=Agents,dc=FBI,dc=org" -description "FBI Agents" 
# Option #2
Set-ADObject "ou=Agents,dc=FBI,dc=org" -replace @{ManagedBy="cn=Walter Skinner,ou=Directors,dc=fbi,dc=org"}
# Option #3
$newou = Get-ADObject "ou=Agents,dc=FBI,dc=org"
$newou.ManagedBy = "cn=Walter Skinner,ou=Directors,dc=fbi,dc=org"
Set-ADObject -instance $newou

"Ergebnis:"
$ou = Get-ADObject  "ou=Agents,dc=FBI,dc=org"
$ou | fl

# ==========
# Skriptende
# ==========




# Der folgende Befehl liefert aus dem aktuellen Active-Directory-Pfad die ersten fünf Einträge, deren Name mit f* beginnt.
Get-ADObject -Filter 'Name -like "f*"' -SearchScope SubTree -resultSetsize 5

# Der folgende Befehl liefert alle Einträge, was durch $null bei resultSetsize anzugeben ist
# (im Standard würden sonst nur die ersten 1000 ausgegeben!).
Get-ADObject -Filter 'Name -like "f*"' -SearchScope SubTree -resultSetsize $null

# Der folgende Befehl listet alle Einträge auf, in denen der Vorname mit „F" und der Nachname mit „M" beginnt.
# ACHTUNG: Man muss die LDAP-Attributnamen (z.?B. "sn") verwenden, nicht die Attributnamen der PowerShell (wie "surname").

Get-ADObject -Filter 'givenname -like "f*" -and sn -like "m*"' -SearchScope SubTree -resultSetsize $null


# Anstelle der moduleigenen Filtersyntax kann man auch die LDAP-Suchsprache verwenden. 
# Das folgende Beispiel sucht korrekt in allen Benutzerkonten, die mit „F" beginnen.

Get-ADObject -LDAPFilter '(&(objectCategory=person)(objectClass=user)(name=f*))' -SearchScope SubTree -resultSetsize $null 
# Die gleichzeitige Verwendung von objectCategory und objectClass in der Suchanfrage steigert die Leistung.
# Vergleichsoperatoren  in
# https://blogs.msdn.microsoft.com/adpowershell/2009/04/14/active-directory-powershell-advanced-filter-part-ii/ 


# PowerShell Syntax deutlich pflegeleichter als die LDAP-Syntax
$date = (Get-date) - (New-Timespan -days 5)
Get-ADUser -Filter { lastLogon -gt $date }
# Mit der LDAP-Syntax wäre dies (die Zeitangabe erfolgt in Einheiten zu 100 Nanosekunden seit dem 1.1.1601):
Get-ADUser -LDAPFilter "(&(lastLogon>=128812906535515110) (objectClass=user) (!(objectClass=computer)))"



# Verwaltung von Organisationseinheiten
# =====================================
# Zur Verwaltung von Organisationseinheiten stehen folgende spezielle Commandlets zur Verfügung:
# Get-ADOrganizationalUnit
# New-ADOrganizationalUnit
# Remove-ADOrganizationalUnit
# Set-ADOrganizationalUnit



# Listing 54.23 Beispielskript für den Einsatz der "OU"-Commandlets  
# WPS2_ADS_OUManagement.ps1]

"Delete and Recreate an OU....."

$ou = Get-ADObject "ou=Directors,dc=FBI,dc=org" 
$ou | fl

set-adobject "ou=Directors,dc=FBI,dc=org" -protectedFromAccidentalDeletion $false
Remove-ADOrganizationalUnit "ou=Directors,dc=FBI,dc=org" -confirm:$false -recursive

"Create OU"
New-ADOrganizationalUnit -ProtectedFromAccidentalDeletion $false -name "Directors" -Path "dc=FBI,dc=org"

"Move an OU..."
Move-ADObject -Identity "ou=Directors,dc=FBI,dc=org" -targetpath "ou=Aliens,dc=FBI,dc=org"
Move-ADObject -Identity "ou=Directors,ou=Aliens, dc=FBI,dc=org" -targetpath "dc=FBI,dc=org"

Rename-ADObject "ou=Directors,dc=FBI,dc=org" -newname "FBI-Directors"

Set-ADOrganizationalUnit "ou=FBI-Directors,dc=FBI,dc=org" -ManagedBy "cn=Fox Mulder,ou=Agents,dc=fbi,dc=org"

"Ergebnis:"
$ou = Get-ADObject "ou=FBI-Directors,dc=FBI,dc=org"
$ou | fl

# ==========
# Skriptende
# ==========



# Verwaltung von Benutzerkonten
# =============================
# Zur Verwaltung von Benutzerkonten stehen im PowerShell-AD-Modul die folgenden Commandlets zur Verfügung:
# Get-ADUser : Benutzerkontenliste oder Daten eines Benutzerkontos
# New-ADUser : Benutzerkonto anlegen
# Remove-ADUser : Benutzerkonto löschen
# Set-ADUser : Eigenscha!en eines Benutzers festlegen

Get-ADUser JoeTest

# Eigenschaften ermitteln
Get-ADUser JoeTest | Get-Member
# mehr Eigenschaften#
Get-ADUser JoeTest -Properties City, Company, Office | Get-Member
# oder
Get-ADUser JoeTest -Properties *

# Filtern und Suchen
Get-ADUser -searchbase "ou=agents,dc=fbi,dc=org" -Filter 'samaccountname -like "F*"'

# Alle Benutzer einer OU
$oupath = "ou=Testing,dc=firma,dc=local"
Get-ADUser -Searchbase $oupath -Filter "*"


# AD-Commandlets nutzen -ErrorAction SilentlyContinue leider nicht sauberer
# daher:
$ErrorActionPreference = "SilentlyContinue"
$path = "CN=NoOU,DC=firma,DC=local"

$o = Get-ADUser $path -ErrorAction SilentlyContinue
if ($o -eq $null) { "Objekt existiert nicht!" }
else { "Objekt existiert!" }

# Alternativ muss man das Commandlet in Try…Catch einbetten:
$o = $(try {Get-ADUser $path } catch {$null})
if ($o -eq $null) { "Objekt existiert nicht!" }
else { "Objekt existiert!" }



# Für Benutzer Beschreibung und Foto ändern
# Listing 54.24 
# [3_Einsatzgebiete\Verzeichnisdienste\WPSModule\WPS2_SetzeADFoto.ps1]

# Eingabedateien
$benutzer = "FoxMulder"
$fotodatei = "t:\foto.jpg"

# Eingabedatentest
$user = $(try {Get-ADUser $benutzer} catch {$null})

if ($user -eq $null) { Write-Error "Benutzer nicht vorhanden!"; exit }
if (-not (Test-Path $fotodatei)) { Write-Error "Fotodatei nicht vorhanden!"; exit }

# Aktion
"Lese Fotodatei ein..."
$fotodaten = [byte[]](Get-Content $fotodatei -Encoding byte )
"Setze Foto $fotodatei für Benutzer $benutzer..."
Set-ADUser -identity $benutzer -Replace @{thumbnailPhoto=$fotodaten; Description="Benutzer mit Foto"}

# Kontrolle
"Kontrollausgabe:"
Get-ADUser -identity $benutzer -Properties thumbnailPhoto,Description | fl thumbnailPhoto, Description

# ==========
# Skriptende
# ==========



# einfachen Benutzer anlegen mit New-ADUser
# Listing 54.25 Anlegen eines Users unter Angabe des Kennworts im Skripttext  
# [WPS2_ADS_UserManagement.ps1]

# folgendes Beispielskript (bzw. Teile) dann später dann unten auch für Gruppen...

Import-Module ActiveDirectory

$root = "dc=FBI,dc=net"
$oupath = "ou=Agents,dc=FBI,dc=net" 
$ouname = "Agents"

if (Test-Path AD:"$oupath" -WarningAction silentlycontinue)
{
 "OU exists and will be removed..."
 set-adobject $oupath -protectedFromAccidentalDeletion $false
 Remove-ADOrganizationalUnit $oupath -Recursive -confirm:$false
 "Removed!"	
}


New-ADOrganizationalUnit -Name $ouname -path $root
"Created!"
Dir  AD:"$oupath"

$fm = New-ADUser -path $oupath –Name "Fox Mulder" –SamAccountName "FoxMulder" –DisplayName "Fox Mulder" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $false -AccountPassword (ConvertTo-SecureString "I+love+Scully" -AsPlainText -force) -PassThru -PasswordNeverExpires:$true -Description "FBI Agent" -HomePage "www.xfiles.com" -Company "FBI"
New-ADUser -path $oupath –Name "Dana Scully" –SamAccountName "DanaScully" –DisplayName "Dana Scully" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString "I+don't+believe" -AsPlainText -force) -PassThru  -Description "FBI Agent"
New-ADUser -path $oupath –Name "John Doggett" –SamAccountName "JohnDoggett" –DisplayName "John Doggett" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString "I+have+a+secret" -AsPlainText -force) -PassThru
New-ADUser -path $oupath –Name "Monica Reyes" –SamAccountName "MonicaReyes" –DisplayName "Monica Reyes" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString "I+have+a+secret" -AsPlainText -force) -PassThru


"--- Gruppe anlegen..."
New-ADGroup -path $oupath -Name "All Agents" -SamAccountName "AllAgents" -GroupScope Global -GroupCategory Security -Description "All FBI Agents" –PassThru
"--- Mitglieder in die Gruppe aufnehmen..."
Add-ADGroupMember -Identity AllAgents -Members FoxMulder 
Add-ADGroupMember -Identity AllAgents -Members DanaScully 
Add-ADGroupMember -Identity AllAgents -Members JohnDoggett 
Add-ADGroupMember -Identity AllAgents -Members MonicaReyes
Add-ADGroupMember -Identity "Remote Desktop Users" -Members FoxMulder
Add-ADGroupMember -Identity "Remote Desktop Users" -Members DanaScully

# Alternative???

#Get-ADUser -Searchbase $oupath -Filter "*" | Add-ADPrincipalGroupMembership -Identity AllAgents


"--- Gruppenmitglieder:"
Get-ADGroupMember -Identity AllAgents
"--- Mitglied entfernen…"
Remove-ADGroupMember -Identity AllAgents -Members MonicaReyes -Confirm:$false
"--- Gruppenmitglieder:"
Get-ADGroupMember -Identity AllAgents


# ==========
# Skriptende
# ==========




<#
#
# Praxis-Skripte für Domain
# Massenhaft User anlegen!
# =========================
#
#>

# --------------------------------------------------------------------
# Online-Beispiel:
# https://activedirectorypro.com/create-bulk-users-active-directory/ 
# inkl. Download-ZIP powershell_create_bulk_users.zip
# --------------------------------------------------------------------

# CSV: 2 Datensätze
# ----
<#
firstname,middleInitial,lastname,username,email,streetaddress,city,zipcode,state,country,department,password,telephone,jobtitle,company,ou
Joshua,L,Lynch,JLynch,JLynch@activedirectorypro.com,2749 Liberty Street,Dallas,75202,TX,United States,Marketing,Roadfox2209,214-800-4820,Marking Specialist,AD Pro,"CN=Users,DC=ad,DC=activedirectorypro,DC=COM"
Sam,A,smith,ssmith,ssmith@activedirectorypro.com,2749 Liberty Street,Dallas,75202,TX,United States,Marketing,Roadfox2208,214-800-4820,Marking Specialist,AD Pro,"CN=Users,DC=ad,DC=activedirectorypro,DC=COM"
#>

# PowerShell-Skript:
# ------------------

# Import active directory module for running AD cmdlets
Import-Module activedirectory
  
#Store the data from ADUsers.csv in the $ADUsers variable
$ADUsers = Import-csv C:\it\bulk_users1.csv

#Loop through each row containing user details in the CSV file 
foreach ($User in $ADUsers)
{
	#Read user data from each field in each row and assign the data to a variable as below
		
	$Username 	= $User.username
	$Password 	= $User.password
	$Firstname 	= $User.firstname
	$Lastname 	= $User.lastname
	$OU 		= $User.ou #This field refers to the OU the user account is to be created in
    $email      = $User.email
    $streetaddress = $User.streetaddress
    $city       = $User.city
    $zipcode    = $User.zipcode
    $state      = $User.state
    $country    = $User.country
    $telephone  = $User.telephone
    $jobtitle   = $User.jobtitle
    $company    = $User.company
    $department = $User.department
    $Password = $User.Password


	#Check to see if the user already exists in AD
	if (Get-ADUser -F {SamAccountName -eq $Username})
	{
		 #If user does exist, give a warning
		 Write-Warning "A user account with username $Username already exist in Active Directory."
	}
	else
	{
		#User does not exist then proceed to create the new user account
		
        #Account will be created in the OU provided by the $OU variable read from the CSV file
		New-ADUser `
            -SamAccountName $Username `
            -UserPrincipalName "$Username@winadpro.com" `
            -Name "$Firstname $Lastname" `
            -GivenName $Firstname `
            -Surname $Lastname `
            -Enabled $True `
            -DisplayName "$Lastname, $Firstname" `
            -Path $OU `
            -City $city `
            -Company $company `
            -State $state `
            -StreetAddress $streetaddress `
            -OfficePhone $telephone `
            -EmailAddress $email `
            -Title $jobtitle `
            -Department $department `
            -AccountPassword (convertto-securestring $Password -AsPlainText -Force) -ChangePasswordAtLogon $True
            
	}
}






# Beispielskript mit neuen Teilnehmern (OU und Gruppe)
# alle gleiches Kennwort 
# WPS2_ADS_UserCreate_Schulungsteilnehmer.ps1

# Massenanlegen von Schulungsteilnehmern
# (C) Holger Schwichtenberg, www.IT-Visions.de, 2012-2014
# ------------------------------------------------------
clear


$AnzahlBenutzer = 3
$wurzelou = "ou=Teilnehmer,dc=ITVSchulung,dc=local"
$Kennwort = "wps+123"
$gruppenname = "Teilnehmer"
$ErrorActionPreference = "continue"

import-module activedirectory
cd ad:

# ------------ OU löschen und wieder anlegen
# -------------------------------------------------------
Function ReNew-OU([string] $oupfad)
{
if ((Test-Path "AD:\$oupfad" -WarningAction silentlycontinue))
{
 "OU wird gelöscht: " + $oupfad
 Set-ADObject $oupfad -ProtectedFromAccidentalDeletion $false
 Remove-ADObject $oupfad -confirm:$false -Recursive
 }
"OU wird angelegt: " + $oupfad
$parent = split-path $oupfad 
$ouname = (split-path $oupfad -Leaf).Replace("ou=","")
$ou = New-ADOrganizationalUnit  -name $ouname -path $parent -PassThru
"OU ist angelegt: " + $ou.distinguishedname
}

# ------------ Gruppe erzeugen, wenn nicht vorhanden
# -------------------------------------------------------
Function NewIfNotExists-Group($wurzelpfad, $name){
$pfad = join-path  $wurzelpfad "cn=$name"
if (-not (Test-Path "AD:/$pfad"))
    {
    $g = New-ADGroup -path $wurzelpfad -Name $name -SamAccountName  $name -GroupScope Global -GroupCategory Security -Description  $name –PassThru
    "Gruppe angelegt " + $g.DistinguishedName
    }
}

# ------------ Benutzer einer Gruppe hinzufügen, Gruppe ggf. anlegen
# -------------------------------------------------------
function Add-ADGroupMemberEx($wurzelpfad, $gruppename, $mitglied)
{
NewIfNotExists-Group $wurzelpfad $gruppename
"--- Mitglieder in die Gruppe aufnehmen..."
Add-ADGroupMember -Identity $gruppename -Members $mitglied
"Benutzer $mitglied in $gruppename aufgenommen"
}


# ********************* Hauptroutine

"Erstellen der Wurzel-OU"
ReNew-OU $wurzelou

"Anlegen der Benutzer..."

# Hauptschleife für n Elemente
for($i = 1; $i -le $AnzahlBenutzer;$i++) {

$unteroupath = $wurzelou

$vorname = "Vorname #$i"
$Nachname = "Nachname #$i"
# Eigenschaften des Benutzers
$verzeichnisname = "Teilnehmer$i"
$Anzeigename = "Teilnehmer #$i"
$SamAccountName = "Teilnehmer$i"
$kennwort = $Kennwort
$title = ""


# Benutzer anlegen
$benutzerObj = New-ADUser -GivenName $vorname -Surname $Nachname -path $unteroupath –Name $verzeichnisname –SamAccountName $SamAccountName –DisplayName $Anzeigename –Title $title –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString $Kennwort -AsPlainText -force) -PassThru

if ($benutzerObj -ne $null){
    "Benutzer #" +$i + ":" + $Anzeigename + " angelegt: SID=" + $benutzerObj.Sid + " Kennwort=" + $kennwort

    # Gruppe hinzufügen, ggf. Gruppe anlegen
    Add-ADGroupMemberEx  $wurzelou $gruppenname $SamAccountName
	# $ADDomSID = (Get-ADDomain).DomainSID.Value # S-1-5-21-...
    Add-ADGroupMember -Identity "Domain admins" -Members $SamAccountName
	# oder besser:
	# Add-ADGroupMember -Identity "$ADDomSID-512" -Members $SamAccountName
    }
}

"Skript ist fertig!"

# ==========
# Skriptende
# ==========




<#
# Lösung mit Eingabedatei / CSV-Datei mit:
# ========================================
Vorname;Name;Titel;Organisationseinheit;Ort;Gruppe
Fox;Mulder;Agent;Hauptdarsteller;Washington DC;Agenten
Dana;Scully;Agent;Hauptdarsteller;Washington DC;Agenten
John;Doggett;Agent;Nebendarsteller;Washington DC;Agenten
Monica;Reyes;Agent;Nebendarsteller;Washington DC;Agenten
Very;Grey;Alien;Subjekte;Universe;Aliens
#>

# hier das entsprechende Skript

# Massenanlegen von Benutzern aus einer CSV-Datei 
# (C) Holger Schwichtenberg, www.IT-Visions.de, 2012-2013
# ------------------------------------------------------
import-module activedirectory
$ErrorActionPreference = "stop"
clear
cd ad:

#$ErrorActionPreference = "continue"
$Eingabedatei = "C:\WPS\3_Einsatzgebiete\Verzeichnisdienste\WPSModule\neueBenutzer.csv"
$wurzelou = "ou=Xfiles,dc=FBI,dc=local"

# ------------ Kennwort generieren
# -------------------------------------------------------
Function New-Password([int] $Anzahl)
{
$kennwort = ""
$zufallszahlgenerator = New-Object System.Random
for($i=0;$i -lt $Anzahl;$i++) { $kennwort = $kennwort +[char]$zufallszahlgenerator.next(33,127) }
return $kennwort
}

# ------------ OU löschen und wieder anlegen
# -------------------------------------------------------
Function ReNew-OU([string] $oupfad)
{
if ((Test-Path "AD:\$oupfad" -WarningAction silentlycontinue))
{
 "OU wird gelöscht: " + $oupfad
 Set-ADObject $oupfad -ProtectedFromAccidentalDeletion $false
 Remove-ADObject $oupfad -confirm:$false -Recursive
 }
#"OU wird angelegt: " + $oupfad
$parent = split-path $oupfad 
$ouname = (split-path $oupfad -Leaf).Replace("ou=","")
$ou = New-ADOrganizationalUnit  -name $ouname -path $parent -PassThru
"OU angelegt: " + $ou.distinguishedname
}

# ------------ Gruppe erzeugen, wenn nicht vorhanden
# -------------------------------------------------------
Function NewIfNotExists-Group($wurzelpfad, $name){
$pfad = join-path  $wurzelpfad "cn=$name"
if (-not (Test-Path "AD:/$pfad"))
    {
    $g = New-ADGroup -path $wurzelpfad -Name $name  -SamAccountName  $name -GroupScope Global -GroupCategory Security -Description  $name –PassThru
    "Gruppe angelegt: " + $g.DistinguishedName
    }
}

# ------------ Benutzer einer Gruppe hinzufügen, Gruppe ggf. anlegen
# -------------------------------------------------------
function Add-ADGroupMemberEx($wurzelpfad, $gruppename, $mitglied)
{
NewIfNotExists-Group $wurzelpfad $gruppename
# --- Mitglieder in die Gruppe aufnehmen..."
Add-ADGroupMember -Identity $gruppename -Members $mitglied
"Benutzer $mitglied in $gruppename aufgenommen"
}


# ********************* Hauptroutine

"Erstellen der Wurzel-OU"
ReNew-OU $wurzelou

"Einlesen der Benutzerliste..."
$benutzerliste = Import-Csv $Eingabedatei -Delimiter ";"

"Anlegen der Benutzer..."
$i = 0

# Hauptschleife für alle Einträge in der CSV-Datei
foreach($benutzer in $benutzerliste)
{
$i++
Write-host "Benutzer $i" -ForegroundColor Yellow
$unteroupath = join-path $wurzelou "ou=$($benutzer.Organisationseinheit)" 

if (-not (Test-Path "AD:\$unteroupath" -WarningAction silentlycontinue))
{
ReNew-OU $unteroupath
}
else
{
"OU $($benutzer.Organisationseinheit) ist vorhanden!"
}

# Eigenschaften des Benutzers
$verzeichnisname = $benutzer.Vorname + "_" + $benutzer.Name
$Anzeigename = $benutzer.Vorname + " " + $benutzer.Name
$SamAccountName = $benutzer.Vorname.Substring(0,1) + $benutzer.Name
$kennwort = New-Password 13
$title = $benutzer.Titel


# Benutzer anlegen
$benutzerObj = New-ADUser -path $unteroupath –Name $verzeichnisname –SamAccountName $SamAccountName  -GivenName ($benutzer.Vorname) -Surname ($benutzer.Name) –DisplayName $Anzeigename –Title $title –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString $Kennwort -AsPlainText -force) -PassThru
$benutzerObj.City = $benutzer.Ort
Set-ADuser -instance $benutzerObj

if ($benutzerObj -ne $null){
    "Benutzer #" +$i + ":" + $Anzeigename + " angelegt: SID=" + $benutzerObj.Sid + " Kennwort=" + $kennwort

    # Gruppe hinzufügen, ggf. Gruppe anlegen
    Add-ADGroupMemberEx  $wurzelou $benutzer.Gruppe $SamAccountName
    }
}

Write-host "Skript ist fertig!" -ForegroundColor Green

# ==========
# Skriptende
# ==========


# einfaches Löschen der erstellten Objekte:
# -----------------------------------------
# WICHTIG: Rückfrage zum Löschen einbauen!!

# zum Löschen der angelegten OUs / OU-Hierarchien muss
# erst der Schutz vor versehentlichem Löschen entfernt werden
# Tipp: die Aufrufe brauchen mindestens did Mandatory Parameter in Zeile
# hier: -Identity
# Zeilenumbrüche dann - wie gewohnt - mit Backticks `
Set-ADObject -Identity:"OU=Testing,DC=dom2012r2,DC=local" `
	-ProtectedFromAccidentalDeletion:$false `
	-Server:"domvbox-2012r2.dom2012r2.local"

# Danach kann -Recursive (nicht -Recurse) gelöscht werden!
Remove-ADObject -Confirm:$false -Identity:"OU=Testing,DC=dom2012r2,DC=local" `
	-Recursive:$true `
	-Server:"domvbox-2012r2.dom2012r2.local"



	
# Praxisbeispiel: Setzen der Anmeldearbeitsstationen (Logon Workstations)
# Listing 54.27 [3_Einsatzgebiete\Verzeichnisdienste\WPSModule\ 
# WPS2_ADS_LogonWorkstations.ps1]


# HS 6.1.2016
# Das folgende Skript Setzt die LogonWorkstations ("Log on To") für alle Mitarbeiter in einer OU ("Mitarbeiter") auf alle Computer, die sich in der OU "PC_Mitarbeiter" befinden. 

$ErrorActionPreference = 'stop'
$oupathBenutzer = 'ou=Mitarbeiter,dc=fbi,dc=org'
$oupathPCs = 'ou=PCs,dc=fbi,dc=org'

# Mitarbeiter ermitteln
$mliste = Get-ADUser -SearchBase $oupathBenutzer -Filter * -SearchScope OneLevel
# Computer ermitteln
$pcliste = Get-ADComputer -SearchBase $oupathPCs -Filter * -SearchScope OneLevel

'=== Erstellen der PC-Liste'

$pclisteCSV = ''
foreach($pc in $pcliste)
{
# Hinweis: Ein Komma am Ende stört in diesem Fall nicht!
$pclisteCSV = $pc.name + ',' + $pclisteCSV
}

'=== Anwenden der PC-Liste auf alle Benutzer'

foreach($m in $mliste)
{
    'Setze PC-Liste für ' + $m.name
    Set-ADUser -Identity $m.DistinguishedName -LogonWorkstations $pclisteCSV
}

# ==========
# Skriptende
# ==========





# Verwaltung von Benutzergruppen
# ==============================
# Get-ADGroup: Benutzergruppen au?isten
# New-ADGroup: Anlegen einer Benutzergruppe
# Set-ADGroup: Eigenscha!en einer Benutzergruppe setzen
# Remove-ADGroup: eine Benutzergruppe entfernen
# Get-ADGroupMember: Auflisten der Mitglieder einer Benutzergruppe
# Get-ADPrincipalGroupMembership:  Au?isten  der  direkten  Mitglieder  einer  Benutzergruppe
# Add-ADGroupMember: Hinzufügen eines Gruppenmitglieds
# Remove-ADGroupMember: Entfernen eines Gruppenmitglieds


# Listing 54.28 
# Verwaltung von Benutzergruppen [WPS2_ADS_UserManagement.ps1] 

Import-Module ActiveDirectory

$root = "dc=FBI,dc=net"
$oupath = "ou=Agents,dc=FBI,dc=net" 
$ouname = "Agents"

if (Test-Path AD:"$oupath" -WarningAction silentlycontinue)
{
 "OU exists and will be removed..."
 set-adobject $oupath -protectedFromAccidentalDeletion $false
 Remove-ADOrganizationalUnit $oupath -Recursive -confirm:$false
 "Removed!"	
}


New-ADOrganizationalUnit -Name $ouname -path $root
"Created!"
Dir  AD:"$oupath"

$fm = New-ADUser -path $oupath –Name "Fox Mulder" –SamAccountName "FoxMulder" –DisplayName "Fox Mulder" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $false -AccountPassword (ConvertTo-SecureString "I+love+Scully" -AsPlainText -force) -PassThru -PasswordNeverExpires:$true -Description "FBI Agent" -HomePage "www.xfiles.com" -Company "FBI"
New-ADUser -path $oupath –Name "Dana Scully" –SamAccountName "DanaScully" –DisplayName "Dana Scully" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString "I+don't+believe" -AsPlainText -force) -PassThru  -Description "FBI Agent"
New-ADUser -path $oupath –Name "John Doggett" –SamAccountName "JohnDoggett" –DisplayName "John Doggett" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString "I+have+a+secret" -AsPlainText -force) -PassThru
New-ADUser -path $oupath –Name "Monica Reyes" –SamAccountName "MonicaReyes" –DisplayName "Monica Reyes" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString "I+have+a+secret" -AsPlainText -force) -PassThru


"--- Gruppe anlegen..."
New-ADGroup -path $oupath -Name "All Agents" -SamAccountName "AllAgents" -GroupScope Global -GroupCategory Security -Description "All FBI Agents" –PassThru
"--- Mitglieder in die Gruppe aufnehmen..."
Add-ADGroupMember -Identity AllAgents -Members FoxMulder 
Add-ADGroupMember -Identity AllAgents -Members DanaScully 
Add-ADGroupMember -Identity AllAgents -Members JohnDoggett 
Add-ADGroupMember -Identity AllAgents -Members MonicaReyes
Add-ADGroupMember -Identity "Remote Desktop Users" -Members FoxMulder
Add-ADGroupMember -Identity "Remote Desktop Users" -Members DanaScully

# Alternative???

#Get-ADUser -Searchbase $oupath -Filter "*" | Add-ADPrincipalGroupMembership -Identity AllAgents


"--- Gruppenmitglieder:"
Get-ADGroupMember -Identity AllAgents
"--- Mitglied entfernen…"
Remove-ADGroupMember -Identity AllAgents -Members MonicaReyes -Confirm:$false
"--- Gruppenmitglieder:"
Get-ADGroupMember -Identity AllAgents

# ==========
# Skriptende
# ==========



# Gruppen auflisten in denen User direkt Mitglied ist
Get-ADPrincipalGroupMembership -Identity FoxMulder 


# Andere Authentifizierung nutzen
# Listing 54.29 
# Abruf des Wurzelobjekts eines Active-Directory-Baums mit einem anderen  Benutzerkonto
$benutzer = "FoxMulder"
$kennwort = ConvertTo-SecureString "I+love+Scully" -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential($benutzer, $kennwort)
Get-ADRootDSE -Credential $cred







<#
#
# Gruppenrichtlinien
# ==================
# (siehe Kapitel 55 PowerShell 5 und Core 6 - Praxisbuch)
# ------------------
# bzw. Buch Gruppenrichtlinien Voges, Dausch mit PowerShell Scripts
# in Form von Modul GroupPolicyHelper.psm1
# 
# Nice compact Website regarding GPO Handling:
# https://cloudbrothers.info/en/manage-group-policies-powershell/
#>

Get-Command -Module GroupPolicy

Get-GPO -all
Get-GPO -Name "Name der GP"
Get-GPO -all | Where-Object { $_.displayname -ilike "d*" }  # ilike - ignore CaseSensitivity

# Datensicherung für alle bestehenden FBI-Gruppenrichtlinien
Get-GPO -all | where { $_.displayname -like "*FBI*" } | Backup-GPO -Path –"c:\wps\GPO_backups"

# WiederherstellenRestore-GPO "GP FBI" -Path "c:\wps\GPO_backups"
Restore-GPO "GP FBI" -Path "c:\wps\GPO_backups"
Restore-GPO -all -Path "c:\wps\GPO_backups"



# Listing 55.2 
# Beispiel zum Anlegen und Verlinken von Gruppenrichtlinien  
# [WPS2_GP_CreateAndLink-GPO.ps1]

Import-Module grouppolicy
# Get-GPO
# exit


# Datensicherung für alle bestehenden FBI-Gruppenrichtlinien
#Get-GPO -all | where { $_.displayname -like "*FBI*" } | Backup-GPO -Path "c:\wps\GPO_backups"
#Restore-GPO "GP FBI" -Path "c:\wps\GPO_backups"
md ad:\"ou=aliens,dc=fbi,dc=org" -ea silentlycontinue
md ad:\"ou=directors,dc=fbi,dc=org" -ea silentlycontinue
md ad:\"ou=agents,dc=fbi,dc=org" -ea silentlycontinue

# Gruppenrichtlinien löschen
Remove-GPO "GP for FBI Agents" -ea silentlycontinue
Remove-GPO "GP FBI" -ea silentlycontinue
Remove-GPO "GP for FBI Directors" -ea silentlycontinue
Remove-GPO "GP for Aliens" -ea silentlycontinue

# Neue Anlegen
new-gpo -name "GP FBI" -Comment "Standard Policy for all FBI Employees"
new-gpo -name "GP for FBI Directors" -Comment "Standard Policy for all FBI Directors"
new-gpo -name "GP for FBI Agents" -Comment "Standard Policy for all FBI Agents"
Copy-GPO -sourcename "GP FBI" -targetname "GP for Aliens"

# Verknüpfen
New-GPLink -name  "GP FBI" -target "dc=fbi,dc=org" -Linkenabled Yes
New-GPLink -name  "GP for FBI Agents" -target "ou=agents,dc=fbi,dc=org" -Linkenabled Yes
New-GPLink -name  "GP for FBI Directors" -target "ou=directors,dc=fbi,dc=org" -Linkenabled Yes
New-GPLink -name  "GP for Aliens" -target "ou=aliens,dc=fbi,dc=org" -Linkenabled Yes

# Blockieren der Vererbung
Set-GPInheritance -target "ou=agents,dc=fbi,dc=org" -isblocked no
Set-GPInheritance -target "ou=aliens,dc=fbi,dc=org" -isblocked yes


# ==========
# Skriptende
# ==========



# GP Bericht erstellen (RSOP)
Get-GPOReport "GP FBI"  -Reporttype html > c:\wps\go_report.htm

# User/Computer berücksichtigen
Get-GPResultantSetOfPolicy -user "FoxMulder" -computer "F171" -ReportType HTML -path c:\wps\rsop.htm


# Der folgende Befehl verhindert, dass die „Aliens" die Richtlinien „GP FBI" und „Default Domain Policy" erben:
Set-GPInheritance -target "ou=aliens,DC=FBI,DC=net" -isblocked yes

# Setzen von Werten in "Registry / GPO"
Get-Help Set-GPRegistryValue -examples










<#
https://www.windowspro.de/script/get-aduser-set-aduser-benutzer-anlegen-abfragen-aendern-powershell
s.a. https://www.windowspro.de/wolfgang-sommergut/uebersicht-kostenlose-tools-fuer-active-directory (Tools - nicht PS)
#>

# Benutzerkonten filtern:
# -----------------------
Get-ADUser -Filter "Surname -like 'Ber*'"
Get-ADUser -Filter "*"  # oder Get-ADUser -Filter *

# Suche beschränken:
# -----------------
Get-ADUser -Filter "Surname -like 'Ber*'" -SearchBase "OU=xfiles,DC=firma,DC=local"

# Alle Eigenschaften auflisten (Properties)
# -----------------------------------------
Get-ADUser -Filter "Surname -like 'Ber*'" -Properties *

# Abfragen mit Search-ADAccount
# -----------------------------
Search-ADAccount -PasswordNeverExpires -UsersOnly

<# Tipp: Search-ADAccount -STRG+Leertaste
-AccountDisabled
-AccountExpiring <Datum>
-AccountInactive <Datum oder Zeitspanne> (z.B.: -AccountInactive 31 für 31 Tage)
-LockedOut
-PasswordExpired #>


# Attribute von ADuser ändern:
# ----------------------------
Get-ADUser -Filter * -SearchBase "OU=xfiles,DC=firma,DC=local" | Set-ADUser -Manager PHuber
# Hinweis: Kontenblatt Organisation - Vorgesetzter

Get-ADUser -Filter "StreetAddress -eq 'Marsstr. 3'" | Set-ADUser -StreetAddress "Rosenweg 1"






<#
https://activedirectorypro.com/create-bulk-users-active-directory/ 
Anm.: inkl. Video (- a bit boring... - )
Weiteres Beispiel für Laden von Benutzern ins AD per CSV mit PowerShell
Download der Dateien möglich
#>

# CSV:
# ----
<#
firstname,middleInitial,lastname,username,email,streetaddress,city,zipcode,state,country,department,password,telephone,jobtitle,company,ou
Joshua,L,Lynch,JLynch,JLynch@activedirectorypro.com,2749 Liberty Street,Dallas,75202,TX,United States,Marketing,Roadfox2209,214-800-4820,Marking Specialist,AD Pro,"CN=Users,DC=ad,DC=activedirectorypro,DC=COM"
Sam,A,smith,ssmith,ssmith@activedirectorypro.com,2749 Liberty Street,Dallas,75202,TX,United States,Marketing,Roadfox2208,214-800-4820,Marking Specialist,AD Pro,"CN=Users,DC=ad,DC=activedirectorypro,DC=COM"
#>

# PowerShell-Skript:
# ------------------

# Import active directory module for running AD cmdlets
Import-Module activedirectory
  
#Store the data from ADUsers.csv in the $ADUsers variable
$ADUsers = Import-csv C:\it\bulk_users1.csv

#Loop through each row containing user details in the CSV file 
foreach ($User in $ADUsers)
{
	#Read user data from each field in each row and assign the data to a variable as below
		
	$Username 	= $User.username
	$Password 	= $User.password
	$Firstname 	= $User.firstname
	$Lastname 	= $User.lastname
	$OU 		= $User.ou #This field refers to the OU the user account is to be created in
    $email      = $User.email
    $streetaddress = $User.streetaddress
    $city       = $User.city
    $zipcode    = $User.zipcode
    $state      = $User.state
    $country    = $User.country
    $telephone  = $User.telephone
    $jobtitle   = $User.jobtitle
    $company    = $User.company
    $department = $User.department
    $Password = $User.Password


	#Check to see if the user already exists in AD
	if (Get-ADUser -F {SamAccountName -eq $Username})
	{
		 #If user does exist, give a warning
		 Write-Warning "A user account with username $Username already exist in Active Directory."
	}
	else
	{
		#User does not exist then proceed to create the new user account
		
        #Account will be created in the OU provided by the $OU variable read from the CSV file
		New-ADUser `
            -SamAccountName $Username `
            -UserPrincipalName "$Username@winadpro.com" `
            -Name "$Firstname $Lastname" `
            -GivenName $Firstname `
            -Surname $Lastname `
            -Enabled $True `
            -DisplayName "$Lastname, $Firstname" `
            -Path $OU `
            -City $city `
            -Company $company `
            -State $state `
            -StreetAddress $streetaddress `
            -OfficePhone $telephone `
            -EmailAddress $email `
            -Title $jobtitle `
            -Department $department `
            -AccountPassword (convertto-securestring $Password -AsPlainText -Force) -ChangePasswordAtLogon $True
            
	}
}










<#
#
# Modul ADDSDeployment
# ====================
# (siehe Kapitel 54.10 PowerShell 5 und Core 6 - Praxisbuch)
#>

# wir benötigen die Rollen (Dienste)
Add-WindowsFeature DNS
Add-WindowsFeature Ad-Domain-Services

# dcpromo - Beispiel von 2016 Server

Import-Module ADDSDeployment
Install-ADDSForest `
-DatabasePath "C:\Windows\NTDS" `
-DomainMode "WinThreshold" `
-DomainName "firma.local" `
-ForestMode "WinThreshold" `
-InstallDNS:$false `
-LogPath "C:\Windows\NTDS" `
-RebootOnCompletion:$false `
-SafeModeAdministratorPassword (Read-Host -AsSecureString -Prompt "Enter Password") `
-SYSVOLPath "C:\Windows\SYSVOL"


# Test-Cmdlets
Get-Command -Module ADDSDeployment Test-ADDS*



<#
#
# Infos zum AD
# ====================
# (siehe Kapitel 54.11 PowerShell 5 und Core 6 - Praxisbuch)
#>

# Module ADPowerShell
# ===================

Get-Command -Module ActiveDirectory Get-AD*



# System.DirectoryServices.ActiveDirectory
# alias Active Directory Management Objects – ADMO
# ===================

# Listing 54.32 
# Informationen über die Domäne und den Forest 
# [ADS_Domain_Info.ps1]

# Aktuelle Domain ermitteln
$d = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain();

# Informationen über aktuelle Domäne
"Name: " + $d.Name
"Domain Mode: " + $d.DomainMode
"Inhaber der InfrastructureRole: " + $d.InfrastructureRoleOwner.Name
"Inhaber der PdcRole: " + $d.PdcRoleOwner.Name
"Inhaber der PdcRole: " + $d.PdcRoleOwner.Name

# Informationen über Forest der aktuellen Domäne
$f = $d.Forest;
"Name der Gesamtstruktur: " + $f.Name
"Modus der Gesamtstruktur: " + $f.ForestMode



# Listing 54.33 
# Informationen über die Domänencontroller und ihre Rollen  
# [ADS_Domaincontroller_Info.ps1]

# Display current domain
$d = [System.Directoryservices.ActiveDirectory.Domain]::GetCurrentDomain()
$DCs = $d.DomainControllers
# Loop over all domain controllers 
foreach ($DC in $DCs)
{
   "Name: " + $DC.Name
   "IP: " + $DC.IPAddress.ToString()
   "Time: " + $DC.CurrentTime.ToString()
    "Roles:"
   # Loop over all roles of DC
    foreach ($R in $DC.Roles)
    {
     "- " + $R.ToString()
    }
}









# Dot.NET Klassen aus Namensraum System.DirectoryServices
# ===============
# viele Beispiel auch im Cookbook
# ausführlich Darstellungen in den Büchern Weltner, 
# Schwichtenberg - PowerShell 5 und Core 6 - Kap. 54.2 ab S.869ff

$o = New-Object system.directoryservices.directoryEntry("LDAP://vm-dc-2016") 
# Hierfür gibt es auch eine Kurzform über den integrierten PowerShell-Datentyp [ADSI]:
$o = [ADSI] "LDAP://vm-dc-2016"

 
# Verzeichnisobjekt existiert?
$janein = [system.directoryservices.directoryEntry]::Exists("LDAP://XFilesServer1/CN=Fox Mulder,OU=Agents,DC=FBI,DC=net")
# kurz:
$janein = [ADSI]::Exists("LDAP://XFilesServer1/CN=Fox Mulder,OU=Agents,DC=FBI,DC=net")


# Listing 54.1 
# Auslesen eines Verzeichnisobjekts [ADS_Einzelobjekte.ps1]

$o = New-Object system.directoryservices.directoryEntry("LDAP://XFilesServer1/CN=Fox Mulder,OU=Agents,DC=FBI,DC=net") 
"Name: "+ $o.sn
"Ort: " + $o.l
"Telefon: " + $o.Telephonenumber
"Weitere Rufnummern: " + $o.OtherTelephone


# Listing 54.2 
# Ändern eines Verzeichnisobjekts [ADS_Einzelobjekte.ps1]

$o.Telephonenumber = "+49 201 7490700"
$o.OtherTelephone = "+01 111 222222","+01 111 333333","+49 111 44444"
$o.SetInfo()
# oder:
$o.PSBase.CommitChanges()


# Listing 54.3 Zugriff auf Basiseigenschaften eines Verzeichnisobjekts  
# [3_Einsatzgebiete\VerzeichnisdiensteADS_User_Misc.ps1]

$o = New-Object system.directoryservices.directoryEntry("LDAP://XFilesServer1/CN=Fox Mulder,OU=Agents,DC=FBI,DC=net", "FoxMulder", "I+love+Scully")
"Klasse: " + $o.PSBase.SchemaClassName
"GUID: " + $o.PSBase.Guid



# Listing 54.4 
# Liste der Unterobjekte eines Containers [ADS_Container_List.ps1]
$pfad= "LDAP://xfilesserver/OU=Agents,DC=FBI,DC=net"
$con = new-object system.directoryservices.directoryEntry($pfad)
$con.PSBase.Children

# Trick
"Das zweite Element ist " + @($con.PSBase.Children)[1].distinguishedName

# Methode/Funktion find()
"Suche nach einem Element " + $con.PSBase.Children.find("cn=Dr. Holger Schwichtenberg").distinguishedName





# Listing 54.5 
# Anlegen einer Organisationseinheit [ADS_OU_DeleteAndCreate.ps1]
# Skript zum Neuanlegen einer OU (Die OU wird geloescht, wenn sie schon existiert!)

$oupfad= "LDAP://xfilesserver/ou=Directors,DC=FBI,DC=net"
$ou = new-object system.directoryservices.directoryEntry($oupfad)
if ([system.directoryservices.directoryEntry]::Exists($oupfad))
{
"OU existiert schon und wird jetzt erst gel?scht!"
$ou.PSBase.DeleteTree()
}

"Anlegen einer OU..."
$pfad= "LDAP://xfilesserver/DC=FBI,DC=net"
$con = new-object system.directoryservices.directoryEntry($pfad)
$ou = $con.PSBase.Children.Add("ou=Directors","organizationalUnit")
$ou.PSBase.CommitChanges()
$ou.Description = "FBI Directors"
$ou.PSBase.CommitChanges()
"OU wurde angelegt!"


# Anm.: Verzeichnisattribute - Dokumentation MSDN
# https://msdn.microsoft.com/en-us/library/ms676817(v=vs.85).aspx (Locality-Name)
# dort dann nach "LDAP-Display-Name" schauen
# Übung: User anlegen mit Register Adresse - Ort (z.B. Musterhausen)
# im ADSI-Editor bei der Eigenschaft "l" zu finden!


# Benutzer anlegen
# ================


# ADS-Benutzer anlegen
$pfad= "LDAP://XFilesServer1/OU=Directors,DC=FBI,DC=net"
$name = "Walter Skinner"
$NTname = "WalterSkinner"
$ou = New-Object DirectoryServices.DirectoryEntry($pfad)
$user = $ou.PSBase.Children.Add("CN=" + $name,'user')
$user.PSBase.CommitChanges() 
$user.SAMAccountName = $NTname
$user.l = "Washington"
$user.Description = "FBI Director"
$user.PSBase.CommitChanges() 

"Benutzer wurde angelegt: " + $user.PBase.Path
$user.SetPassword("secret-123")
"Kennwort wurde gesetzt"
$user.Accountdisabled = $false 
$user.PSBase.CommitChanges() 
"Benutzer wurde aktiviert!"


# Listing 54.9 
# Authentifizierung beim Active Directory 
# [ADS_Authentication.ps1]

Function Authenticate-User2 {
"Versuche, Benutzer " + $args[1] + " mit dem Kennwort " + $args[2] + " zu authentifizieren bei " + $args[0] + "..."
$o = new-object system.directoryservices.directoryEntry([string]$args[0], [String]$args[1], [String]$args[2]) 
$o.PSBase.NativeGUID
}

Function Authenticate-User {
trap [System.Exception] { "Error!"; return $false; }
Authenticate-User2 $args[0], $args[1], $args[2]
return $true
}

#$o = new-object system.directoryservices.directoryEntry("LDAP://E02")
#$o.get_NativeGUID()
$e = Authenticate-User "WinNT://e45" "e45\demo" "demo"
$e
if ($e) { "Benutzer konnte authentifiziert werden!" }
else { "Benutzer konnte NICHT authentifiziert werden!" }



# Benutzer löschen
# ================
# Listing 54.10 
# Löschen eines Benutzers [ADS_User_Create.ps1]

$pfad= "LDAP://XFilesServer1/CN=Fox Mulder,OU=Agents,DC=FBI,DC=net"
$benutzer = New-Object system.directoryservices.directoryEntry($pfad)
if ([system.directoryservices.directoryEntry]::Exists($pfad))
{
"Benutzer existiert schon und wird jetzt erst gelöscht!"
$benutzer.PSBase.DeleteTree()
}




# Benutzer umbenennen
# ===================
# Listing 54.11 
# Umbenennen eines AD-Benutzerkontos [ADS_User_Misc.ps1]

# Benutzer umbenennen
$pfad= "LDAP://XFilesServer1/CN=Dana Scully,OU=Directors,DC=FBI,DC=net"
$user = New-Object system.directoryservices.directoryEntry($pfad)
$user.PSBase.Rename("cn=Dana Mulder")
"Benutzer wurde umbenannt!"


# Benutzer verschieben
# ====================
# Listing 54.12 
# Verschieben eines AD-Benutzerkontos [ADS_User_Misc.ps1]
# Benutzer verschieben
$pfad= "LDAP://XFilesServer1/CN=Walter Fox Mulder,OU=Agents,DC=FBI,DC=net"
$ziel = "LDAP://XFilesServer1/CN=Users,DC=FBI,DC=net "
$user = New-Object system.directoryservices.directoryEntry($pfad)
$user.PSBase.MoveTo($ziel)
"Objekt verschoben!" 




# Gruppen verwalten
# =================

# direkte Mitglieder auflisten
(New-Object directoryservices.directoryentry("LDAP://XFilesServer1/CN=All Agents,DC=FBI,DC=net")).member


# Alle Mitglieder rekursive auflisten
# Listing 54.13 
# Auflisten indirekter Gruppenmitglieder [ADS_Group_Members.ps1]

#######################################
# PowerShell Script
# Display all direct and indirect members of a group
# (C) Dr. Holger Schwichtenberg
# http://www.powershell-doktor.de
########################################

#(new-object  directoryservices.directoryentry("LDAP://xfilesserver/CN=All FBI Employees,DC=FBI,DC=net")).member

"Direct Group Members:"
$group = New-Object directoryservices.directoryentry("LDAP://xfilesserver/CN=All FBI Employees,DC=FBI,DC=net")
$group.member

function Get-Members ($group){
  if ($group.objectclass[1] -eq 'group') { 
   "-- Gruppe $($group.cn)" 
    $Group.member | foreach-object { 
      $de = new-object directoryservices.directoryentry("LDAP://xfilesserver/" + $_) 
      if ($de.objectclass[1] -eq 'group') { 
        Get-Members $de 
      } 
      Else { 
        $de.distinguishedName 
      } 
    } 
  }
  Else {
    Throw "$group is not a group."
  } 
} 

"--- Listing of Group Members:"
"All Members (including non-direct):"
Get-Members(new-object directoryservices.directoryentry("LDAP://xfilesserver/CN=All Employees,DC=FBI,DC=net"))



# Gruppe erstellen und managen
# ============================

#######################################
# PowerShell Script
# (C) Dr. Holger Schwichtenberg
# http://www.powershell-doktor.de
########################################

# Skript zum Neuanlegen einer OU (Die OU wird gelöscht, wenn sie schon existiert!)

$pfad= "LDAP://xfilesserver/cn=All Directors,DC=FBI,DC=net"
$gr = new-object system.directoryservices.directoryEntry($pfad)
if ([system.directoryservices.directoryEntry]::Exists($pfad))
{
"Gruppe existiert schon und wird jetzt erst gel?scht!"
$gr.PSBase.DeleteTree()
}

"Anlegen einer Gruppe..."
$pfad= "LDAP://xfilesserver/DC=FBI,DC=net"
$con = new-object system.directoryservices.directoryEntry($pfad)
$ou = $con.PSBase.Children.Add("cn=All Directors","group")
$ou.PSBase.CommitChanges()
$ou.samaccountname = "AllDirector"
$ou.Description = "Group for FBI Directors"
$ou.PSBase.CommitChanges()
"Gruppe wurde angelegt!"

# Hinzufuegen eines Gruppenmitglieds
$pfad= "LDAP://xfilesserver/cn=All Directors,DC=FBI,DC=net"
$gr = new-object system.directoryservices.directoryEntry($pfad)
$Benutzer = "LDAP://xfilesserver/CN=Walter Skinner,OU=Directors,DC=FBI,DC=net"
$gr.Add($Benutzer)
"Benutzer " + $Benutzer + " wurde der Gruppe " + $ou + " hinzugefuegt!"

# Entfernen eines Gruppenmitglieds
$pfad= "LDAP://xfilesserver/cn=All Directors,DC=FBI,DC=net"
$gr = new-object system.directoryservices.directoryEntry($pfad)
$Benutzer = "LDAP://xfilesserver/CN=Walter Skinner,OU=Directors,DC=FBI,DC=net"
$gr.Remove($Benutzer)
"Benutzer " + $Benutzer + " wurde aus der Gruppe " + $ou + " entfernt!"


# einfache Lösung mit Tools aus Modul PSCX
Test-UserGroupMembership -Identity hs -GroupName Administrators



# Suchen im AD
# ===============
# Syntax:
# Wurzel - Beispiel: LDAP://XFilesServer101/dc=FBI,dc=net
# Filter - Beispiel: (&(objectclass=user)(name=h*))
# Tipp für Filter: Gespeicherte Abfragen im dsa.msc erstellen und Filtern analysieren!
# Attribute - Beispiel: AdsPath ,Name,SamAccountname
# Geltungsbereich
# BASE - auf Ebene suchen
# ONELEVEL - dem Eintrag untergeordnete Einträge durchsuchen
# SUBTREE - alle darunter liegenden Ebenen durchsuchen


# Listing 54.18 
# Ausführen einer LDAP-Suche im AD 
# [ADS_Search_NamePattern.ps1]

$Wurzel = "LDAP://DC=IT-Visions,DC=local"
$Filter = "(&( objectclass=user)(name=h*))"
$Attribute = "CN","ObjectClass","ObjectCategory","distinguishedName","lastLogonTimestamp","description","department","displayname"

# Suche zusammenstellen
$Searcher = New-Object DirectoryServices.DirectorySearcher $Wurzel
$searcher.PageSize = 900 
$searcher.Filter = $Filter
$searcher.SearchScope = "subtree"
$Attribute | foreach {[void]$searcher.PropertiesToLoad.Add($_)} 
# Suche ausführen
$searcher.findAll()
# oder Ausgabe alternativ mit:
$ergebnis = $searcher.findAll()
"Anzahl der Ergebnisse: " + $ergebnis.Count
$ergebnis 




# Listing 54.19 
# Verzeichnisdiensteintrag zu einem Benutzer suchen, dessen SAMAccountName bekannt ist 
# [ADS_Search_SamAccountName.ps1]

$Benutzername = "FoxMulder"
"Suche Benutzer " + $benutzername + "..."
$Wurzel = new-object system.directoryservices.directoryEntry("LDAP://xfilesserver/DC=FBI,DC=net", "FoxMulder", "I+love+Scully")
$Filter = "(SAMAccountName=" + $benutzername +")"
$Attribute = "CN","ObjectClass","ObjectCategory","distinguishedName","lastLogonTimestamp","description","department","displayname"

# Suche zusammenstellen
$Searcher = New-Object DirectoryServices.DirectorySearcher $Wurzel
$searcher.PageSize = 900 
$searcher.Filter = $Filter
$searcher.SearchScope = "subtree"
$Attribute | foreach {[void]$searcher.PropertiesToLoad.Add($_)} 
# Suche ausführen
$searcher.findAll() 



# Tipps & Tricks zur Suche
# ========================
# möglichst * vermeiden am Anfang von Suchwerten
# 
# Suche für verschiedene Verzeichnisklassen:
# Kontakte: (&(objectclass=contact) (objectcategory=person)
# Benutzer: (&(objectclass=user) (objectcategory=person)
# Gruppen: (&(objectclass=group) (objectcategory=group)
# Organisationseinheiten: (&(objectclass=organizationalUnit) (objectcategory=organizationalUnit)
# Computer: (&(objectclass=user) (objectcategory=computer)


# Beispiele für LDAP Suchanfragen
# ===============================
# siehe S. 897 in PowerShell 5 und Core 6, Schwichtenberg

# Get-ADObject aus Modul PSCX siehe S.898

# PSCX - PSProvider DirectoryServices kann neues Laufwerk erstellen:
# Anm.: Laufwerk wurde früher automatisch durch PSCX erstellt!
New-PSDrive -Name firma -Provider DirectoryServices -Root "LDAP://vm-dc-2016/DC=firma,DC=local"
# und dann nutzen mit
dir firma:/users | where { ($_.name -match "domäne") -and ($_.Type -match "group") } | sort name
# oder
New-Item -path firma://Testing00 -type organizationalunit




# Hinweis zu Commandlets von it-visons-Team Schwichtenberg
# https://www.it-visions.de/Scripting/PowerShell/powershellcommandletextensions.aspx 
# Current Version: 2.3.2 (April 2015)
# Download nur mit E-Mail-Bestätigung








# ... TO BE CONTINUED ...
# ... S. 891





# PowerShell Active Directory Papierkorb
# ======================================

# Allgemeine Links:
# https://www.der-windows-papst.de/2016/03/01/active-directory-papierkorb-powershell-befehle/ 
# https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/dd379481%28v%3dws.10%29 

# Aktuelle Doku von Microsoft:
# ============================
# https://docs.microsoft.com/de-de/windows-server/identity/ad-ds/get-started/adac/advanced-ad-ds-management-using-active-directory-administrative-center--level-200- 

# über DSAC (Administrative Center) 
# oder natürlich über die PowerShell aktivieren:

Enable-ADOptionalFeature –Identity "CN=Recycle Bin Feature,CN=Optional Features,CN=Directory Service,CN=Windows NT,CN=Services,CN=Configuration,DC=firma,DC=local" –Scope ForestOrConfigurationSet –Target firma.local

# Überprüfen ob User Test1 vorhanden ist
Get-ADUser -Identity Test1

# User Test1 markieren und löschen
Get-ADUser -Identity Test1 | Remove-ADUser -Confirm:$false

# Überprüfen ob User Test1 nach dem Löschen vorhanden ist
Get-ADUser -Identity Test1

# User Test1 im Papierkorb suchen
Get-ADObject -Filter {Name -like "Test1*"} -IncludeDeletedObjects

# User Test1 wiederherstellen
Get-ADObject -Filter {displayName -eq "Test1"} -IncludeDeletedObjects | Restore-ADObject

Get-ADObject -Filter {displayName -eq "Test1"} -IncludeDeletedObjects | Restore-ADObject -TargetPath "OU=Users,OU=Konfiguration,DC=NDSEDV,DC=DE"

# User Test1 wiederherstellen
Restore-ADObject –identity cf0856c4-1563-4d8b-90c0-46066f1fe1b5

# User Details auslesen
Get-ADObject -Filter {samaccountname -eq "Test1"} -IncludeDeletedObjects -Properties *

# User Details anzeigen lassen GUID ObjectClass OU
Get-ADObject -Filter {Name -like "Test1"}

# Details aller User anzeigen lassen GUID ObjectClass OU
Get-ADObject -filter {ObjectClass -like "user"}

Get-ADObject -filter {ObjectClass -like "user"} -IncludeDeletedObjects | ogv

# User Test1 suchen und in den Ursprungspfad wiederherstellen
Get-ADObject -filter 'samaccountname -eq "Test1"' -IncludeDeletedObjects -properties * | Foreach-Object {Restore-ADObject $_.objectguid -NewName $_.samaccountname -TargetPath $_.LastKnownParent}




# WMI/CIM-Klassen für AD
# ======================
# zu beachten: Get-WmiObject für Remote/WinRM
# Get-CimInstance kennt kein -credential
# also z.B. Invoke-Command benutzen
# Anm.: Möglichkeiten sehr eingeschränkt!


# Alle Konten
Get-CimInstance Win32_Account
# bzw.
Invoke-Command -ComputerName domvbox2012r2 -Credential $cred -SkriptBlock { ... }
# Nur die Benutzerkonten erreicht man mit:
Get-CimInstance Win32_UserAccount
# Nur die Gruppen erreicht man mit:
Get-CimInstance Win32_Group
# gezielt Objekte herausfiltern:
# Name und Domäne der Benutzerkonten, deren Kennwort niemals verfällt
Get-CimInstance Win32_useraccount | Where-Object {$_.Kennwortexpires -eq 0 } | Select-Object Name, Domain
# alternativ:
Get-CimInstance Win32_Useraccount -filter "Kennwortexpires=’false’" | Select-Object Name, Domain
# ob Benutzer „FBI\FoxMulder" einen Bildschirmschoner auf dem Computer „AgentPC04" aktiviert hat.
Get-CimInstance Win32_Desktop -computer AgentPC04 | where { $_.Name -eq "DBI\FoxMulder" } | select screensaveractive

#Get-WmiObject win32_group
Get-wmiobject -Class ds_group -Namespace root\directory\ldap  -Filter "DS_name like 'm%'"







<# Sammlung: Eventlog
   ==================
   zum Seminar PowerShell AD
#>


Get-Eventlog   # interaktiv

# Problem mit Source
Get-EventLog -LogName system | Where-Object { $_.Source -eq "Kernel" } | Select-Object -first 10
# besser
Get-EventLog system | Where-Object { $_.Source -eq "Microsoft-Windows-Kernel-General" –and $_.EventID –eq 12 } | Select-Object -first 10

# EventLog aus den letzten 12 Stunden
Get-EventLog -LogName System -EntryType Error -After (Get-Date).AddHours(-12)


# allgemein:
# Cmdlet Get-WmiObject (hier Adminkonto benötigt und Credential)
$cred = Get-Credential win10\joebadmin
Get-WmiObject -Class Win32_BIOS -ComputerName win10 -Credential $cred

# Anm.: Get-Process, Get-EventLog machen ohne Domäne Probleme
Get-HotFix -ComputerName win10 -Credential $cred


# aus Weltner S. 480ff.
# komplette Funktionsdefinition mit Anfangsblock / Kommentierungen
# =============================

function Get-CriticalEvent
{
<#
    .SYNOPSIS
        listet Fehler und Warnungen aus dem System-Ereignisprotokoll auf
    .DESCRIPTION
        liefert Fehler und Warnungen der letzten 48 Stunden aus dem 
        System-Ereignisprotokoll,
        die auf Wunsch in einem GridView angezeigt werden. Der Beobachtungszeitraum
        kann mit dem Parameter -Hours geändert werden.
    .PARAMETER  Hours
        Anzahl der Stunden des Beobachtungszeitraums. Vorgabe ist 48.
    .PARAMETER  ShowWindow
        Wenn dieser Switch-Parameter angegeben wird, erscheint das Ergebnis in einem
        eigenen Fenster und wird nicht in die Konsole ausgegeben
    .EXAMPLE
        Get-CriticalEvent
        liefert Fehler und Warnungen der letzten 48 Stunden aus dem 
        System-Ereignisprotokoll
    .EXAMPLE
        Get-CriticalEvent -Hours 100
        liefert Fehler und Warnungen der letzten 100 Stunden aus dem 
        System-Ereignisprotokoll
    .EXAMPLE
        Get-CriticalEvent -Hours 24 -ShowWindow
        liefert Fehler und Warnungen der letzten 24 Stunden aus dem 
        System-Ereignisprotokoll und stellt sie in einem eigenen Fenster dar
    .NOTES
        Dies ist ein Beispiel aus Tobias Weltners' PowerShell Buch
    .LINK
        http://www.powertheshell.com
#>
param(
    [int32]    $Hours = 48, 
    [Switch]   $ShowWindow
    )
  if ($ShowWindow)
  {
    Set-Alias Out-Default Out-GridView
  }

  $Heute = Get-Date
  $Differenz = New-TimeSpan -Hours $Hours
  $Stichtag = $Heute - $Differenz

  Get-EventLog -LogName System -EntryType Error, Warning -After $Stichtag |
    Select-Object -Property TimeGenerated, Message | Out-Default
}
















<# Online-Beispiele für
   AD Verwaltungen mit PowerShell
   ==============================   
   eine "kleine" Sammlung
   ==============================
#>




<#
https://blog.netwrix.com/2017/07/20/powershell-active-directory-optimization/

Table of Contents (stay tuned for updates):
#>

# 1. How to Find Inactive Computers in Active Directory Using PowerShell
# Easily obtain a list of inactive computer accounts in Active Directory with the help of PowerShell script.

$DaysInactive = 90
$time = (Get-Date).Adddays(-($DaysInactive))
Get-ADComputer -Filter {LastLogonTimeStamp -lt $time} -ResultPageSize 2000 -resultSetSize $null -Properties Name, OperatingSystem, SamAccountName, DistinguishedName

Get-ADComputer -Filter {LastLogonTimeStamp -lt $time} -ResultPageSize 2000 -resultSetSize $null -Properties Name, OperatingSystem, SamAccountName, DistinguishedName | Export-CSV "C:\Temp\StaleComps.CSV" –NoTypeInformation


# 2. How to Find Locked-Out User Accounts in Active Directory Using PowerShell
# Learn how to query Active Directory for locked-out user accounts in just one step.

Search-ADAccount -LockedOut -UsersOnly -ResultPageSize 2000 -resultSetSize $null | Select-Object Name, SamAccountName, DistinguishedName | Export-CSV "C:\Temp\LockedOutUsers.CSV" -NoTypeInformation


# 3. How to Get a List of Expired User Accounts in AD Using PowerShell
# Check out a simple PowerShell script that will help you to generate a list of expired user accounts in Active Directory.

Search-ADAccount -Server $ThisDomain -Credential $Creds -AccountExpired -UsersOnly -ResultPageSize 2000 -resultSetSize $null| Select-Object Name, SamAccountName, DistinguishedName


# 4. How to Get a List of AD Users Whose Passwords Never Expire Using PowerShell
# Need to make sure that unwanted AD user accounts do not have the "Password Never Expires" attribute set? Use a few PowerShell commands from this article.

Search-ADAccount -PasswordNeverExpires -UsersOnly -ResultPageSize 2000 -resultSetSize $null | Select-Object Name, SamAccountName, DistinguishedName | Export-CSV "C:\Temp\PassNeverExpiresUsers.CSV" -NoTypeInformation


# 5. How to Collect AD Site Information Using PowerShell
# Find various AD site information such as AD site location, site options configured, ISTG assigned to the site, and more by using a bunch of powerful cmdlets.

$ReportFile = "C:\Temp\ADSiteInfo.CSV"
Remove-item $ReportFile -ErrorAction SilentlyContinue
$ThisString="AD Site,Location,Site Option,Current ISTG,Subnets,Servers,In Site Links,Bridgehead Servers"
Add-Content "$ReportFile" $ThisString

$CurForestName = "NetWrix.com"
$a = new-object System.DirectoryServices.ActiveDirectory.DirectoryContext("Forest", $CurForestName)
[array]$ADSites=[System.DirectoryServices.ActiveDirectory.Forest]::GetForest($a).sites
$ADSites
ForEach ($Site in $ADSites)
{
    $SiteName = $Site.Name
    $SiteLocation = $site.Location
    $SiteOption = $Site.Options
    $SiteISTG = $Site.InterSiteTopologyGenerator

    [array] $SiteServers = $Site.Servers.Count
    [array] $SiteSubnets = $Site.Subnets.Count
    [array] $SiteLinks = $Site.SiteLinks.Count
    [array] $SiteBH = $Site.BridgeheadServers.Count

    $FinalVal=$SiteName+","+'"'+$SiteLocation+'"'+","+'"'+$SiteOptions+'"'+","+$SiteISTG+","+$SiteSubnets+","+$SiteServers+","+$SiteLinks+","+$SiteBH
    Add-Content "$ReportFile" $FinalVal          
}



# 6. How to Find Disabled or Inactive Users and Computers in Active Directory Using PowerShell
# Use a handy PowerShell script to find disabled or inactive user and computer accounts in Active Directory.

Get-ADComputer -Filter {(Enabled -eq $False)} -ResultPageSize 2000 -ResultSetSize $null -Server <AnyDomainController> -Properties Name, OperatingSystem
Get-ADComputer -Filter {(Enabled -eq $False)} -ResultPageSize 2000 -ResultSetSize $null -Server <AnyDomainController> -Properties Name, OperatingSystem | Export-CSV "C:\Temp\DisabledComps.CSV" -NoTypeInformation

Search-ADAccount –AccountDisabled –UsersOnly –ResultPageSize 2000 –ResultSetSize $null | Select-Object SamAccountName, DistinguishedName
Search-ADAccount –AccountDisabled –UsersOnly –ResultPageSize 2000 –ResultSetSize $null | Select-Object SamAccountName, DistinguishedName | Export-CSV "C:\Temp\DisabledUsers.CSV" -NoTypeInformation

Search-ADAccount –AccountInActive –TimeSpan 90:00:00:00 –ResultPageSize 2000 –ResultSetSize $null | ?{$_.Enabled –eq $True} | Select-Object Name, SamAccountName, DistinguishedName | Export-CSV "C:\Temp\InActiveUsers.CSV" –NoTypeInformation



# 7. How to Discover New Users in Active Directory Using PowerShell
# Discover new users added to AD within the last 24 hours and email their credentials by using PowerShell.

# 8. How to Create Active Directory Users in Bulk and Email Their Credentials Using PowerShell
# Create Active Directory accounts in bulk and send them a welcome email in two simple steps by using PowerShell.







<#
http://www.itprotoday.com/management-mobility/top-10-active-directory-tasks-solved-powershell
#>

# Task 1: Reset a User Password
# =======

$new=Read-Host "Enter the new password" -AsSecureString
Set-ADAccountPassword jfrost -NewPassword $new
# als Einzeiler
Set-ADAccountPassword jfrost -NewPassword (ConvertTo-SecureString -AsPlainText -String "P@ssw0rd1z3" -force)
# ggf. noch
Set-ADUser jfrost -ChangePasswordAtLogon $True

# Task 2: Disable and Enable a User Account
# =======

Disable-ADAccount jfrost -whatif # zeigt CN!
Disable-ADAccount jfrost         # führt Deaktivierung durch
# return
Enable-ADAccount jfrost
# und gerne auch für viele
get-aduser -filter "department -eq 'sales'" | disable-adaccount

# Task 3: Unlock a User Account (Entsperren)
# =======

Unlock-ADAccount jfrost 

# Task 4: Delete a User Account
# =======

Remove-ADUser jfrost -whatif
# alle deaktivierten Account in einer OU finden, die 180 ungeändert und diese löschen
Get-aduser -filter "enabled -eq 'false'" -property WhenChanged -SearchBase "OU=Employees, DC=Globomantics,DC=Local" | where {$_.WhenChanged -le (Get-Date).AddDays(-180)} | Remove-ADuser -whatif

# Task 5: Find Empty Groups
# =======

get-adgroup -filter * | where {-Not ($_ | get-adgroupmember)} | Select Name
get-adgroup -filter "members -notlike '*' -AND GroupScope -eq 'Universal'" -SearchBase "OU=Groups,OU=Employees,DC=Globomantics,DC=local" | Select Name,Group* 

# Task 6: Add Members to a Group
# =======

add-adgroupmember "chicago IT" -Members jfrost
Add-ADGroupMember "Chicago Employees" -member (get-aduser -filter "city -eq 'Chicago'") 
Get-ADUser -filter "city -eq 'Chicago'" | foreach {Add-ADGroupMember "Chicago Employees" -Member $_}

# Task 7: Enumerate Members of a Group
# =======

Get-ADGroupMember "Domain Admins"  
Get-ADGroupMember "Chicago All Users" -Recursive | Select DistinguishedName

# andere Richtung: Gruppen für user (die Builtin-Gruppen werden nicht gelistet!)
get-aduser jfrost -property Memberof | Select -ExpandProperty memberOf


# Task 8: Find Obsolete Computer Accounts
# =======
# ... siehe online ...

# Task 9: Disable a Computer Account
# =======
# ... siehe online ...


# Task 10: Find Computers by Type
# ========

Get-ADComputer -Filter * -Properties OperatingSystem | Select OperatingSystem -unique | Sort OperatingSystem
Get-ADComputer -Filter "OperatingSystem -like '*Server*'" -properties OperatingSystem, OperatingSystemServicePack | Select Name, Op* | format-list 


# Task ++: Benutzer Password-Alter Report
# ========

Get-ADUser -Filter "Enabled -eq 'True' -AND PasswordNeverExpires -eq 'False'" -Properties PasswordLastSet,PasswordNeverExpires,PasswordExpired | 
	Select DistinguishedName,Name,pass*,@{Name="PasswordAge";Expression={(Get-Date)-$_.PasswordLastSet}} | 
	sort PasswordAge -Descending | ConvertTo-Html -Title "Password Age Report" | Out-File c:\Work\pwage.htm 



	
<# 
https://www.powershellgallery.com/packages/Get-ActiveUser/1.3
als Modul - hier die Funktion:
#>


<#
.Synopsis
   Retrive list of active users on windows machine
.DESCRIPTION
   Uses WMI, CIM or Query.exe
.EXAMPLE
PS C:\> Get-ActiveUser localhost -Method Query
 
ComputerName UserName
------------ --------
localhost jonas
localhost test
 
.EXAMPLE
PS C:\> Get-ActiveUser localhost -Method wmi
 
ComputerName UserName
------------ --------
localhost Jonas
 
.EXAMPLE
PS C:\> Start-Multithread -Script {param($C) Get-ActiveUser -ComputerName $C -Method Query} -ComputerName ::1,Localhost | Out-GridView
 
.NOTES
   This module was created with a powershell.org blogpost in mind
   http://powershell.org/wp/2015/08/28/list-users-logged-on-to-your-machines/
   Created by Jonas Sommer Nielsen
 
#>
function Get-ActiveUser
{
    [CmdletBinding(DefaultParameterSetName='Standard Parameters', 
                SupportsShouldProcess=$true, 
                PositionalBinding=$false,
                HelpUri = 'https://github.com/mrhvid/Get-ActiveUser',
                ConfirmImpact='Medium')]
    [Alias()]
    [OutputType([string[]])]
    Param
    (
        # Computer name, IP, Hostname
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [String]
        $ComputerName,

        # Choose method, WMI, CIM or Query
        [Parameter(Mandatory=$true,
            ValueFromPipelineByPropertyName=$true,
            Position=1)]
        [ValidateSet('WMI','CIM','Query')]
        [String]
        $Method
    )

    Begin
    {
    }
    Process
    {
        Write-Verbose "$Method selected as method"

        switch ($Method)
        {
            'WMI' 
            {
                Write-Verbose "Contacting $ComputerName via WMI"
          
                $WMI = (Get-WmiObject Win32_LoggedOnUser).Antecedent
                $ActiveUsers = @()
                foreach($User in $WMI) {
                    $StartOfUsername = $User.LastIndexOf('=') + 2
                    $EndOfUsername = $User.Length - $User.LastIndexOf('=') -3
                    $ActiveUsers += $User.Substring($StartOfUsername,$EndOfUsername)
                }
                $ActiveUsers = $ActiveUsers | Select-Object -Unique

            }
            'CIM' 
            {
                Write-Verbose "Contacting $ComputerName via CIM"
                $ActiveUsers = (Get-CimInstance Win32_LoggedOnUser -ComputerName $ComputerName).antecedent.name | Select-Object -Unique

            }
            'Query' 
            {
                Write-Verbose "Contacting $ComputerName via Query"
                $Template = @'
 USERNAME SESSIONNAME ID STATE IDLE TIME LOGON TIME
>{USER*:jonas}                 console             1  Active    1+00:27  24-08-2015 22:22
 {USER*:test} 2 Disc 1+00:27 25-08-2015 08:26
>{USER*:mrhvid}                rdp-tcp#2           2  Active          .  9/1/2015 8:54 PM
'@

                $Query = query.exe user /server $ComputerName
                $ActiveUsers = $Query | ConvertFrom-String -TemplateContent $Template | Select-Object -ExpandProperty User
            }

        }

        # Create nice output format
        $UsersComputersToOutput = @()
        foreach($User in $ActiveUsers) {
             $UsersComputersToOutput += New-Object psobject -Property @{ComputerName=$ComputerName;UserName=$User}   
        }

        # output data
        $UsersComputersToOutput
    }
    End
    {
    }
} 



<#
# Hintergrundjobs
# ================
# Weltner Kapitel 25 - Hintergrundjobs und Parallelverarbeitung
#>

Get-Command -Noun Job

# Skript 25.1 
# 3 Jobs nacheinander
# drei Aufgaben definieren
$code1 = { Start-Sleep -Seconds 5; "A" }
$code2 = { Start-Sleep -Seconds 6; "B" }
$code3 = { Start-Sleep -Seconds 7; "C" }

$start = Get-Date

& $code1
& $code2
& $code3

$end = Get-Date
$timespan = $end - $start
$seconds = $timespan.TotalSeconds
Write-Host "Gesamtdauer: $seconds sec."


# jetzt mit Jobs vergleichen
# Skript 25.2
$start = Get-Date

# drei Aufgaben definieren
$code1 = { Start-Sleep -Seconds 5; "A" }
$code2 = { Start-Sleep -Seconds 6; "B" }
$code3 = { Start-Sleep -Seconds 7; "C" }

# zwei Aufgaben in Hintergrundjobs verlagern und dort ausführen:
$job1 = Start-Job -ScriptBlock $code1 
$job2 = Start-Job -ScriptBlock $code2 

# die voraussichtlich längste Aufgabe in der eigenen PowerShell ausführen:
$result3 = & $code3 

# warten, bis alle Hintergrundjobs ihre Aufgabe erledigt haben:
$alljobs = Wait-Job $job1, $job2 

# Ergebnisse der Hintergrundjobs abfragen:
$result1 = Receive-Job $job1
$result2 = Receive-Job $job2

# Hintergrundjobs wieder entfernen
Remove-Job $alljobs

$end = Get-Date

# Ergebnisse ausgeben
$result1, $result2, $result3

$timespan = $end - $start
$seconds = $timespan.TotalSeconds
Write-Host "Gesamtdauer: $seconds sec."      # in der Theorie: Zeit des längsten Skripte - hier 7sec
# real etwas länger, wegen Overhead - Kommunikation zwischen Hosts

# Anmerkungen:
# ============
# Achtung: Hintergrundjobs, die viele Ergebnisse liefern, können Skriptabläufe sogar verlangsamen!
# Am Besten: nur Statusmeldung

# Empfehlungen:
# Langwierige Aufgaben - Overhead dann vernachlässigbar
# Wenig Ergebnisse - diese müssten serialisierte werden für Austausch zwischen PSSessions
# Kurze Lebensdauer - sonst muss man die PowerShell Session aufrechterhalten (Alternative: Task-Scheduling)
# 


# Integrierte Hintergrundjobs:
# Parameter: AsJob

Get-Command -ParameterName AsJob    # oder
Get-Help * -Parameter AsJob         # findet auch in ungeladenen Modulen Cmdlets



<#
# Remoting
# ================
#>

# Einfache SMB-Zugriffe mit –Path
Get-ChildItem -Path \\win10\testfreigabe\
# natürlich nur, wenn man berechtigt ist auf die Freigabe zugreifen zu dürfen 
# Remoteanmeldung zuvor durchführen. 
net use \\win10\ipc$ * /User:joeb

# Tools mit Remot: systeminfo.exe (Remote hier ein Adminkonten nötig)
systeminfo.exe /s win10 /U win10\joebadmin /P
# der schtasks.exe
schtasks.exe /query /s win10 /U joebadmin /p

# Cmdlets mit Parameter –Computername und ohne Session
Get-Command | Where-Object { $_.parameters.keys -contains "ComputerName" -and $_.parameters.keys -notcontains "Session"}
# Parameter „credential" enthalten, also eine eigene Authentifizierung erlauben.
Get-Command | Where-Object { $_.parameters.keys -contains "credential" }


# Cmdlet Get-WmiObject (hier Adminkonto benötigt und Credential)
$cred = Get-Credential win10\joebadmin
Get-WmiObject -Class Win32_BIOS -ComputerName win10 -Credential $cred

# Anm.: Get-Process, Get-EventLog machen ohne Domäne Probleme
Get-HotFix -ComputerName win10 -Credential $cred

$dienst = Get-WmiObject -Class Win32_service -Filter 'Name="RemoteRegistry"' -ComputerName win10 -Credential $cred
$dienst

# Testskript für Ports 137-139 (Namensauflösung), 445 (SMB) und 5985 (PowerShell Remoting via http)
# PowerShell Skript „Weltner – 22.1.ps1 – Funktion: Test-NetworkPort
function Test-NetworkPort
{
  param
  (
    $ComputerName = $env:COMPUTERNAME,

    [int32[]]
    [Parameter(ValueFromPipeline=$true)]
    $Port = $(137..139 + 445 + 5985),

    [int32]
    $Timeout=1000,

    [switch]
    $AllResults
  )

  process
  {
    $count = 0
    ForEach ($PortNumber in $Port)
    {
      $count ++
      $perc = $count * 100 / $Port.Count
      Write-Progress -Activity "Scanning on \\$ComputerName" -Status "Port $PortNumber" -PercentComplete $perc

      # in PowerShell 2.0 muss [Ordered] entfernt werden
      # dann ist die Reihenfolge der Eigenschaften aber zufällig.
      $result = New-Object PSObject -Property ([Ordered]@{
        Port="$PortNumber"; Open=$False; Type='TCP'; ComputerName=$ComputerName})

      $TCPClient = New-Object System.Net.Sockets.TcpClient
      $Connection = $TCPClient.BeginConnect($ComputerName, $PortNumber, $null, $null)

      try
      {
        if ($Connection.AsyncWaitHandle.WaitOne($Timeout, $false))
        {
          $null = $TCPClient.EndConnect($Connection)
          $result.Open = $true
        }
      }
      catch {} finally { $TCPClient.Close() }

      $result | Where-Object { $AllResults -or $_.Open }
    }
  }
}



<# 

Remoting 
========

#> 


# Invoke-Command
# ==============
# Bei Berechtigungsproblemen immer wieder gerne mit der Credential Lösung.
Invoke-Command -ComputerName Win10 -scriptblock { Get-Service b* }
Invoke-Command -ComputerName Win10 -scriptblock { Get-Service | sort status | Format-Table name,status }
Invoke-Command - ComputerName Win10 -Script { "Computername: " + [System.Environment]::MachineName ; "Zeit: " + [DateTime]::Now ; "Sprache: " + (Get-Culture) }
Invoke-Command - ComputerName Win10 -Script { ping www.it-visions.de }
# Gerne natürlich auch ein entsprechendes Skript als Datei - aber ExecutionPolicy bedenken:
Invoke-Command -ComputerName Win10 -scriptblock { c:\temp\WPS2_Computername.ps1 }


# PowerShell Remoting (WinRM)
# ===========================

# Unterdrücken der Nachfragen und Problem mit „Öffentlichen Netzwerkadaptern" umgehen
Enable-PSRemoting –SkipNetworkProfileCheck –Force

# Einfacher Test
Test-WSMan -ComputerName Win10

# Interaktive Sitzung:
Enter-PSSession –Computername Win10
# komplette Anmeldung inkl. –credential (bei P2P immer nötig!)

# Anm.: bei Zugriff P2P auf Domänenmaschine Problemlösung mit TrustedHosts
# Kurzanleitung (mit Admin-Host):
# Set-Item WSMan:\localhost\Client\TrustedHosts -Value "10.0.2.33, Win10" -Force
# Restart-Service WinRM

$cred = Get-Credential
Enter-PSSession -ComputerName Win10 -credential $cred

# oder:
Enter-PSSession –ComputerName Win10 -Authentication Negotiate -credential Win10\joebadmin
# aktuelle Konsolenmaschine:
[System.Environment]::MachineName

# Beenden der PowerShell Remote Session:
Exit-PSSession


# Session Management
# ==================

# Befehle:
Get-Command *PSSession* 
 
# ab S. 229 Schwichtenberg PowerShell 4.0 
# ab S. 742 Weltner PowerShell 5.0


# Testscript Weltner 22.1
# Testskript findet heraus, ob Ports

# *  137-139 (Namensauflösung),
# *  445 (SMB) und
# *  5985 (PowerShell Remoting via http)

# verfügbar sind (**Weltner – 22.1.ps1 – Funktion: Test-NetworkPort**).

function Test-NetworkPort
{
  param
  (
    $ComputerName = $env:COMPUTERNAME,

    [int32[]]
    [Parameter(ValueFromPipeline=$true)]
    $Port = $(137..139 + 445 + 5985),

    [int32]
    $Timeout=1000,

    [switch]
    $AllResults
  )

  process
  {
    $count = 0
    ForEach ($PortNumber in $Port)
    {
      $count ++
      $perc = $count * 100 / $Port.Count
      Write-Progress -Activity "Scanning on \\$ComputerName" -Status "Port $PortNumber" -PercentComplete $perc

      # in PowerShell 2.0 muss [Ordered] entfernt werden
      # dann ist die Reihenfolge der Eigenschaften aber zufällig.
      $result = New-Object PSObject -Property ([Ordered]@{
        Port="$PortNumber"; Open=$False; Type='TCP'; ComputerName=$ComputerName})

      $TCPClient = New-Object System.Net.Sockets.TcpClient
      $Connection = $TCPClient.BeginConnect($ComputerName, $PortNumber, $null, $null)

      try
      {
        if ($Connection.AsyncWaitHandle.WaitOne($Timeout, $false))
        {
          $null = $TCPClient.EndConnect($Connection)
          $result.Open = $true
        }
      }
      catch {} finally { $TCPClient.Close() }

      $result | Where-Object { $AllResults -or $_.Open }
    }
  }
}



# Remote Tools aus RSAT (Remote Server Administration Tools)
# ==========================================================

# Modul ActiveDirectory
Get-ADUser -Filter { Name -like "*Joe*" } 
# mehr unten mit Extra-Kapitel zu Modul Active Directory



# PowerShell WebAccess (PSWA)
# ===========================

# Server Rolle IIS und Feature PSWA nötig:
# Rolle "Web Server (IIS)"
# Feature "Windows PowerShell/Windows PowerShell Web Access"
# Gerne auch per PowerShell installieren:
# ggf.: Import-Module ServerManager
Install-WindowsFeature -name web-server, windowspowershellwebaccess

# Installieren/Bereitstellen von Test-Zertifikat
Install-PswaWebApplication -UseTestCertificate
# Anm.: jetzt Server erreichbar unter https://localhost/pswa
# Regel (hier sehr "frei") für die Erreichbarkeit von PSWA erstellen:
Add-PswaAuthorizationRule –UserName * -ComputerName * -ConfigurationName *
Add-PswaAuthorizationRule -usergroupname domain\administrators -ComputerName * -ConfigurationName *

# Konfiguration – hier nur 3 gleichzeitige Verbindungen
# C:\Windows\Web\PowerShellWebAccess\wwwroot\web.config
# <appSettings>
# <add key="maxSessionsAllowedPerUser" value="3"/>
# </appSettings>







<# DIVERSES

https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/dd378937(v=ws.10)#scripts

https://gallery.technet.microsoft.com/scriptcenter/site/search?f%5B0%5D.Type=RootCategory&f%5B0%5D.Value=activedirectory&f%5B0%5D.Text=Active%20Directory 
https://gallery.technet.microsoft.com/scriptcenter/site/search?f%5B0%5D.Type=RootCategory&f%5B0%5D.Value=activedirectory&f%5B0%5D.Text=Active%20Directory
https://gallery.technet.microsoft.com/scriptcenter/Active-Directory-Health-709336cd
https://www.zubairalexander.com/blog/powershell-script-to-monitor-active-directory-health/


https://blogs.technet.microsoft.com/heyscriptingguy/2011/11/30/use-powershell-to-find-and-remove-inactive-active-directory-users/


https://www.computerwoche.de/a/die-besten-powershell-skripte-in-der-praxis,2068107,2

http://www.tomsitpro.com/articles/powershell-active-directory-cmdlets,2-801.html

https://4sysops.com/archives/powershell-script-to-display-information-about-active-directory-users/
https://4sysops.com/archives/create-new-active-directory-users-with-a-powershell-script/

https://carlwebster.com/microsoft-active-directory-health-check-powershell-script-version-2-03/

http://www.lazywinadmin.com/p/scripts.html
https://github.com/lazywinadmin/PowerShell

https://community.spiceworks.com/scripts?category=1

https://www.der-windows-papst.de/2016/03/01/active-directory-papierkorb-powershell-befehle/ 



https://www.red-gate.com/simple-talk/sysadmin/powershell/managing-active-directory-with-powershell/  
Anm.: 11. Nov. 2013 und Hinweise auf Quest PSSnapin

#>
