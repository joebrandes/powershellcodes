﻿
# Verwaltung von Organisationseinheiten
# =====================================
# Zur Verwaltung von Organisationseinheiten stehen folgende spezielle Commandlets zur Verfügung:
# Get-ADOrganizationalUnit
# New-ADOrganizationalUnit
# Remove-ADOrganizationalUnit
# Set-ADOrganizationalUnit



# Listing 54.23 Beispielskript für den Einsatz der "OU"-Commandlets  
# WPS2_ADS_OUManagement.ps1]

"Delete and Recreate an OU....."

$ou = Get-ADObject "ou=Directors,dc=FBI,dc=org" 
$ou | Format-List

set-adobject "ou=Directors,dc=FBI,dc=org" -protectedFromAccidentalDeletion $false
Remove-ADOrganizationalUnit "ou=Directors,dc=FBI,dc=org" -confirm:$false -recursive

"Create OU"
New-ADOrganizationalUnit -ProtectedFromAccidentalDeletion $false -name "Directors" -Path "dc=FBI,dc=org"

"Move an OU..."
Move-ADObject -Identity "ou=Directors,dc=FBI,dc=org" -targetpath "ou=Aliens,dc=FBI,dc=org"
Move-ADObject -Identity "ou=Directors,ou=Aliens, dc=FBI,dc=org" -targetpath "dc=FBI,dc=org"

Rename-ADObject "ou=Directors,dc=FBI,dc=org" -newname "FBI-Directors"

Set-ADOrganizationalUnit "ou=FBI-Directors,dc=FBI,dc=org" -ManagedBy "cn=Fox Mulder,ou=Agents,dc=fbi,dc=org"

"Ergebnis:"
$ou = Get-ADObject "ou=FBI-Directors,dc=FBI,dc=org"
$ou | Format-List

# ==========
# Skriptende
# ==========
