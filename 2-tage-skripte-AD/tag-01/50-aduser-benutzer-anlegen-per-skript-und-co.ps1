﻿
# einfachen Benutzer anlegen mit New-ADUser
# Listing 54.25 Anlegen eines Users unter Angabe des Kennworts im Skripttext  
# [WPS2_ADS_UserManagement.ps1]

# folgendes Beispielskript (bzw. Teile) dann später dann unten auch für Gruppen...

Import-Module ActiveDirectory

$root = "dc=FBI,dc=net"
$oupath = "ou=Agents,dc=FBI,dc=net" 
$ouname = "Agents"

if (Test-Path AD:"$oupath" -WarningAction silentlycontinue)
{
 "OU exists and will be removed..."
 set-adobject $oupath -protectedFromAccidentalDeletion $false
 Remove-ADOrganizationalUnit $oupath -Recursive -confirm:$false
 "Removed!"	
}


New-ADOrganizationalUnit -Name $ouname -path $root
"Created!"
Get-ChildItem  AD:"$oupath"

$fm = New-ADUser -path $oupath –Name "Fox Mulder" –SamAccountName "FoxMulder" –DisplayName "Fox Mulder" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $false -AccountPassword (ConvertTo-SecureString "I+love+Scully" -AsPlainText -force) -PassThru -PasswordNeverExpires:$true -Description "FBI Agent" -HomePage "www.xfiles.com" -Company "FBI"
New-ADUser -path $oupath –Name "Dana Scully" –SamAccountName "DanaScully" –DisplayName "Dana Scully" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString "I+don't+believe" -AsPlainText -force) -PassThru  -Description "FBI Agent"
New-ADUser -path $oupath –Name "John Doggett" –SamAccountName "JohnDoggett" –DisplayName "John Doggett" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString "I+have+a+secret" -AsPlainText -force) -PassThru
New-ADUser -path $oupath –Name "Monica Reyes" –SamAccountName "MonicaReyes" –DisplayName "Monica Reyes" –Title "Agent" –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString "I+have+a+secret" -AsPlainText -force) -PassThru


"--- Gruppe anlegen..."
New-ADGroup -path $oupath -Name "All Agents" -SamAccountName "AllAgents" -GroupScope Global -GroupCategory Security -Description "All FBI Agents" –PassThru
"--- Mitglieder in die Gruppe aufnehmen..."
Add-ADGroupMember -Identity AllAgents -Members FoxMulder 
Add-ADGroupMember -Identity AllAgents -Members DanaScully 
Add-ADGroupMember -Identity AllAgents -Members JohnDoggett 
Add-ADGroupMember -Identity AllAgents -Members MonicaReyes
Add-ADGroupMember -Identity "Remote Desktop Users" -Members FoxMulder
Add-ADGroupMember -Identity "Remote Desktop Users" -Members DanaScully

# Alternative???

#Get-ADUser -Searchbase $oupath -Filter "*" | Add-ADPrincipalGroupMembership -Identity AllAgents


"--- Gruppenmitglieder:"
Get-ADGroupMember -Identity AllAgents
"--- Mitglied entfernen…"
Remove-ADGroupMember -Identity AllAgents -Members MonicaReyes -Confirm:$false
"--- Gruppenmitglieder:"
Get-ADGroupMember -Identity AllAgents


# ==========
# Skriptende
# ==========

