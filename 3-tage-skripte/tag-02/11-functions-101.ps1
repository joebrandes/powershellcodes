<#
# Funktionen bauen
# ================
#>

# erste einfache - unkommentierte - Funktionen
# mit expliziter Deklaration der Parameter in den {}
function Get-JBSmallFiles {
	param ( [int32] $Size = 100000 )
	Get-ChildItem C:\powershell-bu\scripts | Where-Object {$_.Length -lt $Size -and !$_.PSIsContainer} 
}

# man könnte die Parameter auch außerhalb der {} definieren
# Aufruf: Get-JBSmallFiles2 10000
# Empfehlung: die obere Variante mit explizitem param in {} wird empfohlen!
function Get-JBSmallFiles2 ($size) {
	Get-ChildItem C:\powershell-bu\scripts | Where-Object {$_.Length -lt $Size -and !$_.PSIsContainer} 
}
