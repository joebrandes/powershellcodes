﻿
# Für Benutzer Beschreibung und Foto ändern
# Listing 54.24 
# [3_Einsatzgebiete\Verzeichnisdienste\WPSModule\WPS2_SetzeADFoto.ps1]

# Eingabedateien
$benutzer = "joemustermann"
$fotodatei = "c:\foto.jpg"

# Eingabedatentest
$user = $(try {Get-ADUser $benutzer} catch {$null})

if ($user -eq $null) { Write-Error "Benutzer nicht vorhanden!"; exit }
if (-not (Test-Path $fotodatei)) { Write-Error "Fotodatei nicht vorhanden!"; exit }

# Aktion
"Lese Fotodatei ein..."
$fotodaten = [byte[]](Get-Content $fotodatei -Encoding byte )
"Setze Foto $fotodatei für Benutzer $benutzer..."
Set-ADUser -identity $benutzer -Replace @{thumbnailPhoto=$fotodaten; Description="Benutzer mit Foto"}

# Kontrolle
"Kontrollausgabe:"
Get-ADUser -identity $benutzer -Properties thumbnailPhoto,Description | Format-List thumbnailPhoto, Description

# ==========
# Skriptende
# ==========
