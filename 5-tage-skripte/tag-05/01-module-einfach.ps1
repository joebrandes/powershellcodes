﻿<#
# einfache Module bauen
# =====================
#>

# Modulpfade ermitteln
$env:PSModulePath -split ";"
# oder
$env:PSModulePath.split(";")
# persönlicher Modulordner
$env:PSModulePath.split(";")[0]

# folgende Befehle in Umgebung, wo Funktion Get-CriticalEvent bekannt ist!
# Funktion siehe oben...
# Funktionen lassen sich auch mit Get-Content Function:\funktionsname auslesen
$name = 'Get-CriticalEvent'
$path = Split-Path -Path $profile
$code = "function $name { $((Get-Item function:\$name).Definition) }"
New-Item -Path $path\Modules\$name\$name.psm1 -ItemType File -Force -Value $code

# Erster Test für die Verfügbarkeit des neuen Cmdlet / der neuen Funktion per Modul:
Get-Module get-cr* -ListAvailable