﻿
# Listing 54.5 
# Anlegen einer Organisationseinheit [ADS_OU_DeleteAndCreate.ps1]
# Skript zum Neuanlegen einer OU (Die OU wird geloescht, wenn sie schon existiert!)

$oupfad= "LDAP://xfilesserver/ou=Directors,DC=FBI,DC=net"
$ou = new-object system.directoryservices.directoryEntry($oupfad)
if ([system.directoryservices.directoryEntry]::Exists($oupfad))
{
"OU existiert schon und wird jetzt erst gel?scht!"
$ou.PSBase.DeleteTree()
}

"Anlegen einer OU..."
$pfad= "LDAP://xfilesserver/DC=FBI,DC=net"
$con = new-object system.directoryservices.directoryEntry($pfad)
$ou = $con.PSBase.Children.Add("ou=Directors","organizationalUnit")
$ou.PSBase.CommitChanges()
$ou.Description = "FBI Directors"
$ou.PSBase.CommitChanges()
"OU wurde angelegt!"


# Anm.: Verzeichnisattribute - Dokumentation MSDN
# https://msdn.microsoft.com/en-us/library/ms676817(v=vs.85).aspx (Locality-Name)
# dort dann nach "LDAP-Display-Name" schauen
# Übung: User anlegen mit Register Adresse - Ort (z.B. Musterhausen)
# im ADSI-Editor bei der Eigenschaft "l" zu finden!


# Benutzer anlegen
# ================


# ADS-Benutzer anlegen
$pfad= "LDAP://XFilesServer1/OU=Directors,DC=FBI,DC=net"
$name = "Walter Skinner"
$NTname = "WalterSkinner"
$ou = New-Object DirectoryServices.DirectoryEntry($pfad)
$user = $ou.PSBase.Children.Add("CN=" + $name,'user')
$user.PSBase.CommitChanges() 
$user.SAMAccountName = $NTname
$user.l = "Washington"
$user.Description = "FBI Director"
$user.PSBase.CommitChanges() 

"Benutzer wurde angelegt: " + $user.PBase.Path
$user.SetPassword("secret-123")
"Kennwort wurde gesetzt"
$user.Accountdisabled = $false 
$user.PSBase.CommitChanges() 
"Benutzer wurde aktiviert!"


# Listing 54.9 
# Authentifizierung beim Active Directory 
# [ADS_Authentication.ps1]

Function Authenticate-User2 {
"Versuche, Benutzer " + $args[1] + " mit dem Kennwort " + $args[2] + " zu authentifizieren bei " + $args[0] + "..."
$o = new-object system.directoryservices.directoryEntry([string]$args[0], [String]$args[1], [String]$args[2]) 
$o.PSBase.NativeGUID
}

Function Authenticate-User {
trap [System.Exception] { "Error!"; return $false; }
Authenticate-User2 $args[0], $args[1], $args[2]
return $true
}

#$o = new-object system.directoryservices.directoryEntry("LDAP://E02")
#$o.get_NativeGUID()
$e = Authenticate-User "WinNT://e45" "e45\demo" "demo"
$e
if ($e) { "Benutzer konnte authentifiziert werden!" }
else { "Benutzer konnte NICHT authentifiziert werden!" }



# Benutzer löschen
# ================
# Listing 54.10 
# Löschen eines Benutzers [ADS_User_Create.ps1]

$pfad= "LDAP://XFilesServer1/CN=Fox Mulder,OU=Agents,DC=FBI,DC=net"
$benutzer = New-Object system.directoryservices.directoryEntry($pfad)
if ([system.directoryservices.directoryEntry]::Exists($pfad))
{
"Benutzer existiert schon und wird jetzt erst gelöscht!"
$benutzer.PSBase.DeleteTree()
}


