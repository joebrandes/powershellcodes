﻿<#
# Eingaben
# ========
# s.a. Erstellen von UI-Elementen 
# https://docs.microsoft.com/de-de/powershell/scripting/samples/sample-scripts-for-administration?view=powershell-5.1
# z.B.:
# https://docs.microsoft.com/de-de/powershell/scripting/samples/creating-a-graphical-date-picker?view=powershell-7.2
#>

# in Shell: Standardeingaben mittels Read-Host (Typ: System.String)
$name = Read-Host "Bitte Benutzernamen eingeben:"

# Standardeingaben verschlüsselt mittels Read-Host (Typ: System.Security.SecureString)
# kein Mitlesen der Eingabe möglich!
$kennwort_verschluesselt = Read-Host -AsSecureString "Bitte Kennwort eingeben:"

# Das verschlüsselte Kennwort wieder zurückholen
[String]$kennwort_unverschluesselt = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($kennwort_verschluesselt))
"Kennwort: " + $kennwort_unverschluesselt


# Fenster / Forms
# ===============

# GUI: mittels .NET Framework in der Klasse Microsoft.VisualBasic.Interaction
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic")
$eingabe = [Microsoft.VisualBasic.Interaction]::InputBox("Bitte geben Sie Ihren Namen ein!")
"Hallo $Eingabe!"

# Dialogfenster (auch mit Dot.NET)
[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")
[System.Console]::Beep(15440, 30)
[System.Windows.Forms.MessageBox]::Show("Gleich kommt eine Frage","Vorwarnung",
[System.Windows.Forms.MessageBoxButtons]::OK)
$antwort = [System.Windows.Forms.MessageBox]::Show("Nachricht","Ueberschrift", [System.Windows.Forms.MessageBoxButtons]::YesNo)
if ($antwort -eq "Yes") { "Sie haben zugestimmt!" } else { "Sie haben abgelehnt!" }






# Erinnerung: Authentifizierungsdialog mit Get-Credentials
# erzeugt Instanz von System.Management.Automation.PSCredential
Get-Credential
# bzw.:
$cred = Get-Credential -Credential domain\Benutzername
