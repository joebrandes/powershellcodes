<#
# Hintergrundjobs
# ================
# Weltner Kapitel 25 - Hintergrundjobs und Parallelverarbeitung
#>

Get-Command -Noun Job

# Skript 25.1 
# 3 Jobs nacheinander
# drei Aufgaben definieren
$code1 = { Start-Sleep -Seconds 5; "A" }
$code2 = { Start-Sleep -Seconds 6; "B" }
$code3 = { Start-Sleep -Seconds 7; "C" }

$start = Get-Date

& $code1
& $code2
& $code3

$end = Get-Date
$timespan = $end - $start
$seconds = $timespan.TotalSeconds
Write-Host "Gesamtdauer: $seconds sec."


# jetzt mit Jobs vergleichen
# Skript 25.2
$start = Get-Date

# drei Aufgaben definieren
$code1 = { Start-Sleep -Seconds 5; "A" }
$code2 = { Start-Sleep -Seconds 6; "B" }
$code3 = { Start-Sleep -Seconds 7; "C" }

# zwei Aufgaben in Hintergrundjobs verlagern und dort ausführen:
$job1 = Start-Job -ScriptBlock $code1 
$job2 = Start-Job -ScriptBlock $code2 

# die voraussichtlich längste Aufgabe in der eigenen PowerShell ausführen:
$result3 = & $code3 

# warten, bis alle Hintergrundjobs ihre Aufgabe erledigt haben:
$alljobs = Wait-Job $job1, $job2 

# Ergebnisse der Hintergrundjobs abfragen:
$result1 = Receive-Job $job1
$result2 = Receive-Job $job2

# Hintergrundjobs wieder entfernen
Remove-Job $alljobs

$end = Get-Date

# Ergebnisse ausgeben
$result1, $result2, $result3

$timespan = $end - $start
$seconds = $timespan.TotalSeconds
Write-Host "Gesamtdauer: $seconds sec."      # in der Theorie: Zeit des längsten Skripte - hier 7sec
# real etwas länger, wegen Overhead - Kommunikation zwischen Hosts

# Anmerkungen:
# ============
# Achtung: Hintergrundjobs, die viele Ergebnisse liefern, können Skriptabläufe sogar verlangsamen!
# Am Besten: nur Statusmeldung

# Empfehlungen:
# Langwierige Aufgaben - Overhead dann vernachlässigbar
# Wenig Ergebnisse - diese müssten serialisierte werden für Austausch zwischen PSSessions
# Kurze Lebensdauer - sonst muss man die PowerShell Session aufrechterhalten (Alternative: Task-Scheduling)
# 


# Integrierte Hintergrundjobs:
# Parameter: AsJob

Get-Command -ParameterName AsJob    # oder
Get-Help * -Parameter AsJob         # findet auch in ungeladenen Modulen Cmdlets

