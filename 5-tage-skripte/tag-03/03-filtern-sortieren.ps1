﻿<#
# Filtern / Sortieren
# ===================
#>

# Prozesse, der Speicher größer als 10000000 Bytes
Get-Process | Where-Object {$_.WS -gt 10000000 }
Get-Process | Where-Object {$_.WS -gt 10MB }   # hier 1024*1024!

# Inklusive Sortierung und Auswahl der Ergebnissätze
Get-Process | Sort-Object WS -Descending | Select-Object -First 5
Get-Process | Sort-Object WS -Descending | Select-Object -Last 5

# RegEx (Regular Expressions - Reguläre Ausdrücke)
# =====
# Systemdienste, deren Beschreibung aus zwei durch ein Leerzeichen getrennten Wörtern besteht.
Get-Service | Where-Object { $_.DisplayName -match "^\w* \w*$" }
# Prozesse, deren Namen mit einem "i" starten und danach drei Buchstaben
Get-Process | Where-Object { $_.ProcessName -match "^i\w{3}$" }

Get-Service | Where-Object { $_.status -eq "running" }

# seit PowerShell 3.0 auch mit folgender Kurzschreibweise ermittelt werden
Get-Service | Where-Object status -eq "running"

# bei Kombinationen mit and oder or bitte wieder klassische, lange Schreibweisen
Get-Process | 
Where-Object { $_.Name -eq "iexplore" -or $_.name -eq "Chrome" -or $_.name -eq "Firefox" } | 
Stop-Process

Get-Service | Where-Object { $_.status -eq "running" -and $_.name -like "a*" }

# Objekte für Ausgaben einschränken ("kastrieren")
Get-Process | Select-Object processname, get_minworkingset, ws | Get-Member 
# Prozesse nach Speicherverbrauch sortieren
Get-Process | Sort-Object ws –desc
# Mehrere Sortierfelder
Get-Service | Sort-Object Status, Displayname
# Mehrfach vorkommende Elemente finden - man muss immer erst sortieren!
1,5,7,8,5,7 | Sort-Object | Get-Unique
(Get-Process).Name | Sort-Object | Get-Unique
# Elemente gruppieren
Get-Service | Group-Object status
# Dateien in C:\Windows\System32 nach Erweiterungen gruppieren und sortiert ausgeben:
Get-ChildItem c:\windows\system32 -File | 
Group-Object extension | 
Sort-Object count –desc

Get-ChildItem c:\windows\system32 | Select-Object extension -Unique
# Einsatz von Bool
Get-Childitem c:\Windows | Where-Object { !$_.PsIsContainer } | Group-Object { $_.Length -gt 1MB}
# Auswertungen mit Measure-Object - Standard ist count, also Anzahl
Get-ChildItem c:\windows | Measure-Object -Property length -min -max -average -sum

