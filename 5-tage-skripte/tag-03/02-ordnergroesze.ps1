﻿# Größe eines Ordners:
# ====================
$folder = "$env:userprofile\Downloads"
# alternative:
# $folder = "$env:userprofile\Documents"
# $folder = "C:\users\Teilnehmer\Downloads" ist ungeschickt, weil "STARR"
Get-ChildItem -Path $folder -Recurse -Force -ea 0 | 
    Measure-Object -Property Length -Sum | 
    ForEach-Object { 
$sum = $_.Sum / 1MB 
"Der Download-Ordner enthält derzeit {0:#,##0.0} MB storage." -f $sum
}

# -EA 0 meint SilentlyContinue

# PowerShell Registry Listing
Clear-Host
Set-Location HKLM:\
Get-Childitem -EA 0

Researching the about_commonParameters file help file will explain why these also work:
-EA 1 Continue
-EA 2 Inquire
-EA 3 Confirm
-EA 4 Stop
