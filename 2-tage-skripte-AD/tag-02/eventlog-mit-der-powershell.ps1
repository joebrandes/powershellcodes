﻿<# Sammlung: Eventlog
   ==================
   zum Seminar PowerShell AD
#>


Get-Eventlog   # interaktiv

# Problem mit Source
Get-EventLog -LogName system | Where-Object { $_.Source -eq "Kernel" } | Select-Object -first 10
# besser
Get-EventLog system | Where-Object { $_.Source -eq "Microsoft-Windows-Kernel-General" –and $_.EventID –eq 12 } | Select-Object -first 10

# EventLog aus den letzten 12 Stunden
Get-EventLog -LogName System -EntryType Error -After (Get-Date).AddHours(-12)


# allgemein:
# Cmdlet Get-WmiObject (hier Adminkonto benötigt und Credential)
$cred = Get-Credential win10\joebadmin
Get-WmiObject -Class Win32_BIOS -ComputerName win10 -Credential $cred

# Anm.: Get-Process, Get-EventLog machen ohne Domäne Probleme
Get-HotFix -ComputerName win10 -Credential $cred


# aus Weltner S. 480ff.
# komplette Funktionsdefinition mit Anfangsblock / Kommentierungen
# =============================

function Get-CriticalEvent
{
<#
    .SYNOPSIS
        listet Fehler und Warnungen aus dem System-Ereignisprotokoll auf
    .DESCRIPTION
        liefert Fehler und Warnungen der letzten 48 Stunden aus dem 
        System-Ereignisprotokoll,
        die auf Wunsch in einem GridView angezeigt werden. Der Beobachtungszeitraum
        kann mit dem Parameter -Hours geändert werden.
    .PARAMETER  Hours
        Anzahl der Stunden des Beobachtungszeitraums. Vorgabe ist 48.
    .PARAMETER  ShowWindow
        Wenn dieser Switch-Parameter angegeben wird, erscheint das Ergebnis in einem
        eigenen Fenster und wird nicht in die Konsole ausgegeben
    .EXAMPLE
        Get-CriticalEvent
        liefert Fehler und Warnungen der letzten 48 Stunden aus dem 
        System-Ereignisprotokoll
    .EXAMPLE
        Get-CriticalEvent -Hours 100
        liefert Fehler und Warnungen der letzten 100 Stunden aus dem 
        System-Ereignisprotokoll
    .EXAMPLE
        Get-CriticalEvent -Hours 24 -ShowWindow
        liefert Fehler und Warnungen der letzten 24 Stunden aus dem 
        System-Ereignisprotokoll und stellt sie in einem eigenen Fenster dar
    .NOTES
        Dies ist ein Beispiel aus Tobias Weltners' PowerShell Buch
    .LINK
        http://www.powertheshell.com
#>
param(
    [int32]    $Hours = 48, 
    [Switch]   $ShowWindow
    )
  if ($ShowWindow)
  {
    Set-Alias Out-Default Out-GridView
  }

  $Heute = Get-Date
  $Differenz = New-TimeSpan -Hours $Hours
  $Stichtag = $Heute - $Differenz

  Get-EventLog -LogName System -EntryType Error, Warning -After $Stichtag |
    Select-Object -Property TimeGenerated, Message | Out-Default
}

