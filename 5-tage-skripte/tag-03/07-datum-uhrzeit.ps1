﻿<#
# Datum / Uhrzeit
# ===============
#> 

Get-Date
Get-Date -displayhint date
Get-Date -displayhint time

$a = Get-Date "22/1/1972 15:11:10"
$a.DayOfWeek                  # Saturday
Get-Date $a –Format dddd      # Samstag
Get-Date $a -Format "yyyy-MM-dd HH:mm"


$Dauer = New-TimeSpan -Days 10 -hours 4 -minutes 3 -seconds 50
$jetzt = Get-Date
$zukunft = $jetzt + $Dauer

# EventLog aus den letzten 12 Stunden
Get-EventLog -LogName System -EntryType Error -After (Get-Date).AddHours(-12)

# Was kann Get-Date?
Get-Date | Get-Member -MemberType *Method
# Methode nutzen
(Get-Date).ToShortDateString()

# Zeitspanne berechnen
[TimeSpan]5d

# DateTime - statische Methoden
[DateTime]::DaysInMonth(2014, 2)
# 28
[DateTime]::DaysInMonth(2016, 2)
# 29


==================================================================
ab hier functions - für später/morgen/...
==================================================================



# Installzeit (als Unix-Timestamp)
$Path = 'HKLM:\Software\Microsoft\Windows NT\CurrentVersion'
Get-ItemProperty -Path $Path | Select-Object -ExpandProperty InstallDate

# als Echte Zeit in datetime_cookbook.pdf von Weltner:
function ConvertFrom-UnixTime {
param(
    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [Int32] $UnixTime
)
begin {
    $startdate = Get-Date –Date '01/01/1970'
    }
process {
    $timespan = New-TimeSpan -Seconds $UnixTime
    $startdate + $timespan
    }
}

# jetzt die Installzeit in echter Zeit:
$Path = ‘HKLM:\Software\Microsoft\Windows NT\CurrentVersion’
Get-ItemProperty -Path $Path |
Select-Object -ExpandProperty InstallDate |
ConvertFrom-UnixTime

# alternativ: ohne Pipelining
$Path = ‘HKLM:\Software\Microsoft\Windows NT\CurrentVersion’
$installdate = Get-ItemProperty -Path $Path | Select-Object -ExpandProperty InstallDate 
ConvertFrom-UnixTime -UnixTime $installdate


# Install in vergangenen Tagen
$Path = 'HKLM:\Software\Microsoft\Windows NT\CurrentVersion'
Get-ItemProperty -Path $Path | Select-Object -ExpandProperty InstallDate | ConvertFrom-UnixTime | New-TimeSpan | Select-Object -ExpandProperty Days 


# Zeitzonen
[System.TimeZoneInfo]::GetSystemTimeZones()
[System.TimeZoneInfo]::Local
([System.TimeZoneInfo]::Local).StandardName


# lokalisierte Zeitinfos
[System.Enum]::GetNames([System.DayOfWeek])
# deutsch
0..6 | ForEach-Object { [Globalization.DatetimeFormatInfo]::CurrentInfo.DayNames[$_] }
0..11 | ForEach-Object { [Globalization.DatetimeFormatInfo]::CurrentInfo.MonthNames[$_] }

# verfügbare Culture IDs
[System.Globalization.CultureInfo]::GetCultures(‘InstalledWin32Cultures’)
# Übersetzen der IETF Language Tags
[System.Globalization.CultureInfo]::GetCultureInfoByIetfLanguageTag(‘de-DE’)


# Spielerei - Lunch Time für die Titelzeile der Konsole!
function prompt {
$lunchtime = Get-Date -Hour 12 -Minute 30
$timespan = New-TimeSpan -End $lunchtime
[Int]$minutes = $timespan.TotalMinutes
switch ($minutes) {
    { $_ -lt 0 } { $text = ‘Lunch is over. {0}’; continue }
    { $_ -lt 3 } { $text = ‘Prepare for lunch! {0}’ }
default { $text = ‘{1} minutes to go... {0}’ }
}
‘PS> ‘
$Host.UI.RawUI.WindowTitle = $text -f (Get-Location), $minutes
if ($minutes -lt 3) { [System.Console]::Beep() }
}


# noch eine Spielerei mit Uhrzeit in Konsole
function Add-Clock {
$code = {
    $pattern = ‘\d{2}:\d{2}:\d{2}’
    do {
        $clock = Get-Date -Format ‘HH:mm:ss’
        $oldtitle = [system.console]::Title
    if ($oldtitle -match $pattern) {
        $newtitle = $oldtitle -replace $pattern, $clock
        } else {
            $newtitle = “$clock $oldtitle”
            }
        [System.Console]::Title = $newtitle
        Start-Sleep -Seconds 1
        } while ($true)
    }
$ps = [PowerShell]::Create()
$null = $ps.AddScript($code)
$ps.BeginInvoke()
}

# Aufrufen der Funktion
Add-Clock
# auch gerne wieder ohne "Ausgaben"
$null = Add-Clock
