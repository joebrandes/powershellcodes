﻿# Dot.NET Klassen aus Namensraum System.DirectoryServices
# ===============
# viele Beispiel auch im Cookbook
# ausführlich Darstellungen in den Büchern Weltner, 
# Schwichtenberg - PowerShell 5 und Core 6 - Kap. 54.2 ab S.869ff

$o = New-Object system.directoryservices.directoryEntry("LDAP://vm-dc-2016") 
# Hierfür gibt es auch eine Kurzform über den integrierten PowerShell-Datentyp [ADSI]:
$o
 
# Verzeichnisobjekt existiert?
$janein = [system.directoryservices.directoryEntry]::Exists("LDAP://XFilesServer1/CN=Fox Mulder,OU=Agents,DC=FBI,DC=net")
# kurz:
$janein = [ADSI]::Exists("LDAP://XFilesServer1/CN=Fox Mulder,OU=Agents,DC=FBI,DC=net")


# Listing 54.1 
# Auslesen eines Verzeichnisobjekts [ADS_Einzelobjekte.ps1]

$o = New-Object system.directoryservices.directoryEntry("LDAP://XFilesServer1/CN=Fox Mulder,OU=Agents,DC=FBI,DC=net") 
"Name: "+ $o.sn
"Ort: " + $o.l
"Telefon: " + $o.Telephonenumber
"Weitere Rufnummern: " + $o.OtherTelephone


# Listing 54.2 
# Ändern eines Verzeichnisobjekts [ADS_Einzelobjekte.ps1]

$o.Telephonenumber = "+49 201 7490700"
$o.OtherTelephone = "+01 111 222222","+01 111 333333","+49 111 44444"
$o.SetInfo()
# oder:
$o.PSBase.CommitChanges()


# Listing 54.3 Zugriff auf Basiseigenschaften eines Verzeichnisobjekts  
# [3_Einsatzgebiete\VerzeichnisdiensteADS_User_Misc.ps1]

$o = New-Object system.directoryservices.directoryEntry("LDAP://XFilesServer1/CN=Fox Mulder,OU=Agents,DC=FBI,DC=net", "FoxMulder", "I+love+Scully")
"Klasse: " + $o.PSBase.SchemaClassName
"GUID: " + $o.PSBase.Guid



# Listing 54.4 
# Liste der Unterobjekte eines Containers [ADS_Container_List.ps1]
$pfad= "LDAP://xfilesserver/OU=Agents,DC=FBI,DC=net"
$con = new-object system.directoryservices.directoryEntry($pfad)
$con.PSBase.Children

# Trick
"Das zweite Element ist " + @($con.PSBase.Children)[1].distinguishedName

# Methode/Funktion find()
"Suche nach einem Element " + $con.PSBase.Children.find("cn=Dr. Holger Schwichtenberg").distinguishedName

