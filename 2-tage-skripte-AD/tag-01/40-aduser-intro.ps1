﻿

# Verwaltung von Benutzerkonten
# =============================
# Zur Verwaltung von Benutzerkonten stehen im PowerShell-AD-Modul die folgenden Commandlets zur Verfügung:
# Get-ADUser : Benutzerkontenliste oder Daten eines Benutzerkontos
# New-ADUser : Benutzerkonto anlegen
# Remove-ADUser : Benutzerkonto löschen
# Set-ADUser : Eigenscha!en eines Benutzers festlegen

Get-ADUser JoeTest

# Eigenschaften ermitteln
Get-ADUser JoeTest | Get-Member
# mehr Eigenschaften#
Get-ADUser JoeTest -Properties City, Company, Office | Get-Member
# oder
Get-ADUser JoeTest -Properties *

# Filtern und Suchen
Get-ADUser -searchbase "ou=agents,dc=fbi,dc=org" -Filter 'samaccountname -like "F*"'

# Alle Benutzer einer OU
$oupath = "ou=Testing,dc=dom-2016,dc=local"
Get-ADUser -Searchbase $oupath -Filter "*"


# AD-Commandlets nutzen -ErrorAction SilentlyContinue leider nicht sauberer
# daher:
$ErrorActionPreference = "SilentlyContinue"
$path = "CN=NoOU,DC=dom-2016,DC=local"

$o = Get-ADUser $path -ErrorAction SilentlyContinue
if ($o -eq $null) { "Objekt existiert nicht!" }
else { "Objekt existiert!" }

# Alternativ muss man das Commandlet in Try…Catch einbetten:
$o = $(try {Get-ADUser $path } catch {$null})
if ($o -eq $null) { "Objekt existiert nicht!" }
else { "Objekt existiert!" }

