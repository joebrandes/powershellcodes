﻿
<#
# Lösung mit Eingabedatei / CSV-Datei mit:
# ========================================
Vorname;Name;Titel;Organisationseinheit;Ort;Gruppe
Fox;Mulder;Agent;Hauptdarsteller;Washington DC;Agenten
Dana;Scully;Agent;Hauptdarsteller;Washington DC;Agenten
John;Doggett;Agent;Nebendarsteller;Washington DC;Agenten
Monica;Reyes;Agent;Nebendarsteller;Washington DC;Agenten
Very;Grey;Alien;Subjekte;Universe;Aliens
#>

# hier das entsprechende Skript

# Massenanlegen von Benutzern aus einer CSV-Datei 
# (C) Holger Schwichtenberg, www.IT-Visions.de, 2012-2013
# ------------------------------------------------------
import-module activedirectory
$ErrorActionPreference = "stop"
clear
cd ad:

#$ErrorActionPreference = "continue"
$Eingabedatei = "C:\WPS\3_Einsatzgebiete\Verzeichnisdienste\WPSModule\neueBenutzer.csv"
$wurzelou = "ou=Xfiles,dc=FBI,dc=local"

# ------------ Kennwort generieren
# -------------------------------------------------------
Function New-Password([int] $Anzahl)
{
$kennwort = ""
$zufallszahlgenerator = New-Object System.Random
for($i=0;$i -lt $Anzahl;$i++) { $kennwort = $kennwort +[char]$zufallszahlgenerator.next(33,127) }
return $kennwort
}

# ------------ OU löschen und wieder anlegen
# -------------------------------------------------------
Function ReNew-OU([string] $oupfad)
{
if ((Test-Path "AD:\$oupfad" -WarningAction silentlycontinue))
{
 "OU wird gelöscht: " + $oupfad
 Set-ADObject $oupfad -ProtectedFromAccidentalDeletion $false
 Remove-ADObject $oupfad -confirm:$false -Recursive
 }
#"OU wird angelegt: " + $oupfad
$parent = split-path $oupfad 
$ouname = (split-path $oupfad -Leaf).Replace("ou=","")
$ou = New-ADOrganizationalUnit  -name $ouname -path $parent -PassThru
"OU angelegt: " + $ou.distinguishedname
}

# ------------ Gruppe erzeugen, wenn nicht vorhanden
# -------------------------------------------------------
Function NewIfNotExists-Group($wurzelpfad, $name){
$pfad = join-path  $wurzelpfad "cn=$name"
if (-not (Test-Path "AD:/$pfad"))
    {
    $g = New-ADGroup -path $wurzelpfad -Name $name  -SamAccountName  $name -GroupScope Global -GroupCategory Security -Description  $name –PassThru
    "Gruppe angelegt: " + $g.DistinguishedName
    }
}

# ------------ Benutzer einer Gruppe hinzufügen, Gruppe ggf. anlegen
# -------------------------------------------------------
function Add-ADGroupMemberEx($wurzelpfad, $gruppename, $mitglied)
{
NewIfNotExists-Group $wurzelpfad $gruppename
# --- Mitglieder in die Gruppe aufnehmen..."
Add-ADGroupMember -Identity $gruppename -Members $mitglied
"Benutzer $mitglied in $gruppename aufgenommen"
}


# ********************* Hauptroutine

"Erstellen der Wurzel-OU"
ReNew-OU $wurzelou

"Einlesen der Benutzerliste..."
$benutzerliste = Import-Csv $Eingabedatei -Delimiter ";"

"Anlegen der Benutzer..."
$i = 0

# Hauptschleife für alle Einträge in der CSV-Datei
foreach($benutzer in $benutzerliste)
{
$i++
Write-host "Benutzer $i" -ForegroundColor Yellow
$unteroupath = join-path $wurzelou "ou=$($benutzer.Organisationseinheit)" 

if (-not (Test-Path "AD:\$unteroupath" -WarningAction silentlycontinue))
{
ReNew-OU $unteroupath
}
else
{
"OU $($benutzer.Organisationseinheit) ist vorhanden!"
}

# Eigenschaften des Benutzers
$verzeichnisname = $benutzer.Vorname + "_" + $benutzer.Name
$Anzeigename = $benutzer.Vorname + " " + $benutzer.Name
$SamAccountName = $benutzer.Vorname.Substring(0,1) + $benutzer.Name
$kennwort = New-Password 13
$title = $benutzer.Titel


# Benutzer anlegen
$benutzerObj = New-ADUser -path $unteroupath –Name $verzeichnisname –SamAccountName $SamAccountName  -GivenName ($benutzer.Vorname) -Surname ($benutzer.Name) –DisplayName $Anzeigename –Title $title –Enabled $true –ChangePasswordAtLogon $true -AccountPassword (ConvertTo-SecureString $Kennwort -AsPlainText -force) -PassThru
$benutzerObj.City = $benutzer.Ort
Set-ADuser -instance $benutzerObj

if ($benutzerObj -ne $null){
    "Benutzer #" +$i + ":" + $Anzeigename + " angelegt: SID=" + $benutzerObj.Sid + " Kennwort=" + $kennwort

    # Gruppe hinzufügen, ggf. Gruppe anlegen
    Add-ADGroupMemberEx  $wurzelou $benutzer.Gruppe $SamAccountName
    }
}

Write-host "Skript ist fertig!" -ForegroundColor Green

# ==========
# Skriptende
# ==========

