﻿
<#
# Variablen (Einführung für die Shell)
# ====================================
#>

Get-Process | Where-Object {$_.name -eq "iexplore"} | Foreach-Object { $_.ws }
# wird zu
$x = Get-Process
$y = $x | Where-Object {$_.name -eq "iexplore"}
$z = $y | Foreach-Object { $_.ws }

# Tee - zeitgleiche Verzweigungen
Get-Service | Tee-Object -Variable a | Where-Object { $_.Status -eq "Running" } | 
    Select-Object name | Tee-Object -FilePath D:\_temp\dienste.txt | Format-Table name

# alternative Lösung:
Get-Service -OutVariable a | Where-Object { $_.Status -eq "Running" } | Select-Object name | Set-Content c:\temp\dienste2.txt -PassThru | Format-Table name
