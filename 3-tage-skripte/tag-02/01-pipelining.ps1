﻿<#
# Pipelining
# ==========
# ALT:  https://entwickler.de/online/powershell-4-0-kraftfutter-fuer-admins-und-entwickler-162759.html 
# NEU:  https://entwickler.de/visual-studio/kraftfutter-fur-admins-und-entwickler
# ABER: nur für Abonnenten lesbar
#>

Get-Process | Format-List 
# komplexere Pipeline:
Get-ChildItem c:\temp -Recurse -Filter *.jpg |
Where-Object { $_.Length -gt 40000 } | 
Select-Object Name, Length |
Sort-Object Length | 
Format-List

Get-Process | Where-Object {$_.name -eq "iexplore"} | Format-Table ProcessName, WorkingSet64

# Alle Prozesse, die mehr als 20 MB verbrauchen:
Get-Process | Where-Object {$_.WorkingSet64 -gt 20*1024*1024 }
# kurze Variante (kann man so nur für die Befehlszeile nutzen! Stichwort: Quick and Dirty)
ps | ? {$_.ws -gt 20MB }

# alle Dienste, die mit i beginnen
"i*" | Get-Service
# Dienst "gleich" BITS
"BITS" | Get-Service

# grep (Textkette filtern) mit der PS
netstat | Select-String "HERGESTELLT" -case         

Get-Date
# Anzahl aller Prozesse 
(Get-Process).count
# Anzahl von Prozessen mit mehr als 20 MB im RAM
(Get-Process | Where-Object { $_.WorkingSet64 -gt 20MB }).Count
# Objekte lassen sich dann auch mit Array-Technik einzeln ansprechen:
(Get-Process | Where-Object { $_.WorkingSet64 -gt 20MB })[5]

# automatic unrolling (ab PowerShell 3)
(Get-Process).Name
# Entspricht den folgenden beiden Aufrufen (abwärtskompatibel dann auch zu PowerShell 2.0)
Get-Process | Select-Object Name
Get-Process | ForEach-Object {$_.Name}

# Beispiel zu automatic unrolling
$dll = Get-ChildItem C:\Windows\System32\*.dll | Select-Object -First 3
$dll    # Ausgabe
# wieder 3 Versionen
$dll.VersionInfo      # ab PS 3.0
$dll | Select-Object -ExpandProperty VersionInfo
$dll | ForEach-Object { $_.VersionInfo }

# Analyse
$dll | Get-Member   # hier sieht man VersionInfo / ScriptProperty – nach der Pipeline!
# im Vergleich mit 
Get-Member –InputObject $dll   # $dll kennt kein VersionInfo

# Pipe nötig, wenn mehrere Member auszugeben sind:
Get-Process | Foreach-Object {$_.Name + ": " + $_.Workingset64 }

# Methods
Get-Process iexplore | Foreach-Object { $_.Kill() }
# besser - ohne Fehler wenn kein iexplore
Get-Process | Where-Object { $_.Name -eq "iexplore" } | Stop-Process

Get-Date | Get-Member
(Get-Date).ToShortDateString()  

Get-Process | Get-Member -Membertype Properties
Get-Process | Get-Member -Membertype Property
Get-Process | Get-Member -Membertype Method
Get-Process | Get-Member *set*
Get-Process | Get-Member *name*

# Eigenschaftssätze (PropertySet - siehe Definitionen wieder in types.ps1ml in $PSHOME)
Get-Process | Select-Object psResources | Format-Table

# Profithema: Analysemöglichkeit beim Pipelining:
Trace-Command -Name ParameterBinding -PSHost -Expression { Get-Childitem c:\temp -filter *.txt | select -First 1 | Get-Content }

# Get-Member kann dann auch helfen Zusammenhang von Cmdlets mit Systemklassen herstellen:
# Vergleiche
# bei Prozessen:
[System.Diagnostics.Process]::GetProcesses() | Sort-Object ProcessName
Get-Process
# bei System-Datum-/Uhrzeit:
[DateTime]::Now
Get-Date        # erstaunlich nicht!?
