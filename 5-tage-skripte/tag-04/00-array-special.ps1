﻿# Hash für Nutzung in Cmdlets
$age = @{
	Name='Alter'
	Expression={ (New-TimeSpan $_.LastWriteTime).TotalDays }
}
# Jetzt $age einsetzen:
Get-ChildItem $env:windir -file | Select-Object Name, $age, Length -First 4
Get-ChildItem $env:windir | Select-Object Name, $age, Length | Where-Object { $_.Alter -lt 5 }