﻿<#
# Kontrollstrukturen
# ==================
#> 

# if
# ==

if ($a -gt 2)
	{
		Write-Host "The value $a is greater than 2."
	}
	elseif ($a -eq 2)
	{
		Write-Host "The value $a is equal to 2."
	}
	else
	{
		Write-Host "The value $a is less than 2 or was not created or initialized."
	}


# switch
# ======

# switch Beispiel
"Welche Bewertung?"
$note = Read-Host
switch ($note)
{
	1 {"sehr gut"}
	2 {"gut"}
	3 {"befriedigend"}
	4 {"ausreichend"}
	5 {"ungenügend"}
	default { "Ungültige Note" }
}


# for
# ===

for ($i=1 ; $i -le 10 ; $i++ ) 
	{ Write-Host $i }

# Schleifen "ineinander" verschachteln:
for ($i=1 ; $i -le 10 ; $i++ ) 
	{ for ($j=1 ; $j -le 10 ; $j++ ) 
        { Write-Host ($i*$j) }
    }

	
# Beispielschleife mit Fakultät:  
# Fakultät 5 = 5! = 1*2*3*4*5	
	
"Bitte eine Zahl eingeben:"
$Fakultaet = Read-Host
$FakultaetErgebnis = 1
for ($i = 1; $i -le $Fakultaet; $i++)
	{
	$FakultaetErgebnis = $FakultaetErgebnis * $i
	}
"Die Fakultät von " + $Fakultaet + " ist " + $FakultaetErgebnis
	
"Bitte eine Zahl eingeben:"
$Fakultaet = Read-Host
$FakultaetErgebnis = 1
$Abbruch = $false
for ($i = 1; $i -lt $Fakultaet; $i++)
	{
	$FakultaetErgebnis = $FakultaetErgebnis * $i
	if ($FakultaetErgebnis -gt [System.Int32]::MaxValue) { $Abbruch = $true; break; }
	}
if ($Abbruch) { "Werteüberlauf!" }
else { "Die Fakultät von " + $Fakultaet + " ist " + $FakultaetErgebnis }
	
	
# while (Abbruchbedingung am Anfang)
# =====

while($val -ne 3)
	{
		$val++
		Write-Host $val
	}

	
# do ... while (Abbruchbedingung am Ende)
# ============

$a = 0
$count = 0
$x = 1,2,78,0
do { $count++; $a++; } while ($x[$a] -ne 78) 
$count     # ergibt 2
	
	
# do ... until
# ============

$a = 0
$count = 0
$x = 1,2,78,0
do { $count++; $a++; } until ($x[$a] -eq 0) 
$count     # ergibt 3


# foreach
# =======

# Thema „Foreach-Object in Pipeline vs. „foreach im Skript“
# Pipeline-basierter Befehl ForEach-Object (asynchron)
Get-ChildItem c:\Windows -Recurse -Filter "*.txt" |
    ForEach-Object { "{0,-20}: {1}" -f $_.Name , $_.length }

#vs. skriptbasierten Befehlsfolge (foreach-Schleife erst nach Get-ChildItem)
$Dateien = Get-ChildItem -Path C:\Windows -Recurse -Filter "*.txt" -ErrorAction SilentlyContinue
foreach ($datei in $Dateien) {
    "{0,-20}: {1}" -f $datei.Name , $datei.length
}

<# 
Zusammenfassung: (Anm.: Fehler gerne mit -erroraction silentlycontinue unterdrücken)
Der Pipeline-Befehl beginnt sofort mit der Ausgabe der Dateiliste. Die skriptbasierte
Lösung braucht einige Zeit vor der ersten Ausgabe.
Der Pipeline-Befehl meldet zwischen den Ausgaben Fehler über Pfade, für die
es kein Zutrittsrecht gibt. Die skriptbasierte Lösung zeigt erst alle Fehlermeldungen,
die ja von Get-ChildItem kommen, und dann erst die Dateiausgabe,
die aus der ForEach-Schleife stammt. 
#>


$letterArray = "a","b","c","d"
foreach ($letter in $letterArray) {
	Write-Host $letter
	}

foreach ($file in Get-ChildItem) { 
    if ($file.length -gt 100KB) {
        Write-Host $file
        Write-Host $file.length
        Write-Host $file.lastaccesstime
        Write-Host ""
    }
}

foreach ($file in Get-ChildItem) {
	if ($file.length -gt 100KB) {
		# ToString mit F0 meint 0 Dezimalstellen; F2 also mit 2 Dezimalstellen
		Write-Host $file "file size:" ($file.length / 1024).ToString("F0") KB
	}
}

