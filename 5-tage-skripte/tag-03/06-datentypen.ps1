﻿<#
# Typen
# =====
#>

2+3
2..5
# Zahlen einfach implizit zuweisen mit 
$i = 15
$i.GetType().Name
$i.GetType().FullName
$i.GetType().NameSpace
# Analyse mit
$i.GetType() | Get-Member

# Rundung
18 / 5
# 3,6
[Int](18/5)
# 4


# Type holen
$a = 1
$a.GetType().FullName
# System.Int32
$a = 1.87
$a.GetType().FullName
# System.Double
$a = 623876378232
$a.GetType().FullName
# System.Int64
$a = ‘Hello’
$a.GetType().FullName
# System.String
$a = Get-Date
$a.GetType().FullName
# System.DateTime


# Zahlen - Int
[Int32]::MaxValue
# 2147483647
[UInt32]::MaxValue
# 4294967295
[Int32]::MinValue
# -2147483648
[UInt32]::MinValue
# 0

# Zeichen - Charakter
[Char]65
# A
[Byte][Char]’A’
# 65
[Byte][Char]’\’
# 92   
[Byte[]][Char[]]’Hello\’
# 72
#101
#108
#108
#111

[Char[]](65..90)
[Char[]]'Hello'
[Byte[]][Char[]]'Hello'


# Spezieller Typ für Mailadressen
[System.Net.Mail.MailAddress]'Some User<some.person@somewhere.com>'
$email = [System.Net.Mail.MailAddress]'Some User<some.person@somewhere.com>'


# Sortieren mit Typen
1,10,3,2 | Sort-Object
# 1
# 2
# 3
# 10
'1','10','3','2' | Sort-Object
# 1
# 10
# 2
# 3
# type conversion:
'1','10','3','2' | Sort-Object -Property { [Double]$_ }
# 1
# 2
# 3
# 10


# Konvertieren
$binary = ‘1110111000010001’
[System.Convert]::ToInt64($binary, 2)
# 60945

0xFFFF
# 65535


# Methode Großschreibung
$DriveList = 'a', 'b:', 'd', 'Z', 'x:'
$DriveList | ForEach-Object { $_.ToUpper()[0] } | Sort-Object


# Zufallszahlen:
Get-Random -Min 100 -Max 200  # ehemals aus den PS Community Extensions (PSCX) - jetzt PS Utility

# oder Zufallszahlen mit .NET-Klasse System.Random
$rnd = New-Object System.Random
$zufallszahl = $rnd.next(100) + 100
$zufallszahl


# Finde Statische Methoden
# [DateTime]::
[DateTime] | Get-Member -Static
[DateTime]::IsLeapYear(2012)
# Vergleiche:
[DateTime]::Now
Get-Date        # erstaunlich nicht!?


# Statische Eigenschaften nutzen
[System.Environment] | Get-Member -Static -MemberType *property
[System.Environment]::SystemDirectory
[System.Environment]::OSVersion
[System.Environment]::OSVersion.ServicePack    # Null ab Windows 8
[System.Environment]::OSVersion.Version
[System.Environment]::Is64BitOperatingSystem
[System.Environment]::MachineName

[System.Enum]::GetNames([System.Environment+SpecialFolder])
[System.Environment]::GetFolderPath(‘CommonMusic’) 


[System.Net.Dns]::GetHostByName(‘microsoft.com’)
([System.Net.Dns]::GetHostByName(‘microsoft.com’)).AddressList
([System.Net.Dns]::GetHostByName(‘microsoft.com’)).AddressList.IPAddressToString

# Array:
[String] $CSVString = "Joe;Brandes;Braunschweig;Deutschland;seminare.firma.de"
$CSVArray = $CSVString.Split(";")
$Surname = $CSVArray[1]
$Surname
