﻿<#
# Arrays
# ======
#>

# Array definieren
$a = 01,08,72,13,04,76
# Das Array kann auch explizit mit [array] deklariert werden:
[array] $b
$b = 01,08,72,13,04,76
$b.Count

# last element, benutze -1:
$myArray[-1]

# new element mit +=:
$myArray += 'new element'

# Array Inhalte checken
'Peter', 'Mary', 'Martin' -contains 'Mary'
True
'Peter', 'Mary', 'Martin' -contains 'Ma*'
False
'Peter', 'Mary', 'Martin', 'Martin'
True
'Peter', 'Mary', 'Martin' -like 'Ma*'
Mary
Martin
@('Peter', 'Mary', 'Martin' -like 'Ma*').Count -gt 0
True

# Array reverse
$a = 1,2,3,4
[array]::Reverse($a)
$a

# Array of Strings
'hostname' * 50
# vergleiche mit 
@('hostname') * 50


# assoziatives Array
# Implizite Definition Hashtable
$Computers = @{ E01 = "192.168.1.10"; E02 = "192.168.1.20"; E03 = "192.168.1.30"; }
# Explizite Definition Hashtable
[Hashtable] $Computers = @{ E01 = "192.168.1.10"; E02 = "192.168.1.20"; E03 = "192.168.1.30" }
# Ausgaben
$Computers["E02"]
$Computers.E02


# Hash für Nutzung in Cmdlets
$age = @{
	Name='Alter'
	Expression={ (New-TimeSpan $_.LastWriteTime).TotalDays }
}
# Jetzt $age einsetzen:
Get-ChildItem $env:windir | Select-Object Name, $age, Length -First 4
Get-ChildItem $env:windir | Select-Object Name, $age, Length | Where-Object { $_.Alter -lt 5 }


# Hash bauen und Element entfernen
$myHash = @{}
$myHash.Name = 'Joe'
$myHash.ID = 12
$myHash.Location = 'Braunschweig'
$myHash
# remove
$myHash.Remove('Location')
$myHash


# XML-Special:
# ============

# XML für TN aus dem Netz beziehen:
Invoke-WebRequest http://www.welt.de/sport/fussball/?service=Rss -OutFile welt_blog.xml
# sauberes XML erzeugen:
$xml = [xml](Get-Content welt_blog.xml)
# Testaufrufe für XML-Nutzung
$xml
$xml.rss
($xml.rss.channel.item).Count
($xml.rss.channel.item)[0]
