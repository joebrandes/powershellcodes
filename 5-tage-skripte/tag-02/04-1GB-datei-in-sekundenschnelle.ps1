﻿# 1GB Datei in Sekundenbruchteil
# ==============================
$path = ”c:\powershell-bu\testfile.txt”
$file = [io.file]::Create($path)
$file.SetLength(1gb)
$file.Close()
Get-Item $path