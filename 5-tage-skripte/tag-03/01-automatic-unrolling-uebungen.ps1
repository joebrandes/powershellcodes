﻿# automatic unrolling (ab PowerShell 3)
(Get-Process).Name
# Entspricht den folgenden beiden Aufrufen (abwärtskompatibel dann auch zu PowerShell 2.0)
Get-Process | Select-Object Name, ws 
Get-Process | ForEach-Object {$_.Name + " " + $_.ws }

# Beispiel zu automatic unrolling
$dll = Get-ChildItem C:\Windows\System32\*.dll | Select-Object -First 3
$dll    # Ausgabe
# wieder 3 Versionen
$dll.VersionInfo      # ab PS 3.0
$dll | Select-Object -ExpandProperty VersionInfo
$dll | ForEach-Object { $_.VersionInfo }