﻿<#
# Registry Specials
# =================
#>

# siehe auch 
# Weltner Registry_Cookbook.pdf
# Schwichtenberg Praxisbuch Kap. 37 Registrierungsdatenbank (Registry) S. 633 ff.

# Registry-Beispiele 

if (Test-Path hklm:/software/firmenname) { Remove-Item hklm:/software/firmenname }

# 1.	Listen Sie alle Unterschlüssel von HKLM/Software auf.
Get-ChildItem hklm:/software

# 2.	Erzeugen Sie einen neuen Unterschlüssel mit Namen "Firmenname"
new-item hklm:/software/firmenname

# 3.	Wechseln Sie den aktuellen Pfad in den neuen Unterschlüssel.
Set-Location hklm:/software/firmenname

# 4.	Definieren Sie ein neues PowerShell-Laufwerk mit Namen "Firma:" f?r diesen Pfad.
New-PSDrive -Name Firma -PSProvider Registry -Root hklm:\software\Firmenname

# 5.	Wechseln Sie den aktuellen Pfad in das neue Laufwerk.
Set-Location Firma:

# 6.	Legen Sie einen neuen Wert vom Typ REG_SZ zu dem schlüssel an mit den Namen "Zeichenkette1" und dem Wert "ABCDE".
New-Itemproperty -path "Firma:" -name "Zeichenkette1" -value  "ABCDE" -type string

# 7.	Legen Sie einen neuen Wert vom Typ REG_EXPAND_SZ zu dem schlüssel an mit den Namen "Zeichenkette2" und dem Wert "%SystemRoot%/WindowsPowerShell"
New-Itemproperty -path "Firma:" -name "Zeichenkette2" -value  "%SystemRoot%/WindowsPowerShell" -type ExpandString

# 8.	Legen Sie einen neuen Wert vom Typ REG_MULTI_SZ zu dem schlüssel an mit den Namen "Mehrfachzeichenkette" und den Werten "A", "B", "C", "D" und "E".
$Mehrfachzeichenkette = "A", "B", "C", "D", "E"
New-Itemproperty -path "Firma:" -name "Mehrfachzeichenkette" -value  $Mehrfachzeichenkette -type Multistring

# 9.	Legen Sie einen neuen Wert vom Typ REG_DWORD zu dem schlüssel an mit den Namen "Zahl" und dem Dezimalwert 10.
New-Itemproperty -path "Firma:" -name "Zahl" -value  10 -type dword

# 10.	Legen Sie einen neuen Wert vom Typ REG_BINARY zu dem schlüssel an mit den Namen "Binaer" und den Werten 10,11,12,13,14,15.
$Werte = 10,11,12,13,14,15
New-Itemproperty -path "Firma:" -name "Binaer" -value $Werte -type Binary

# 11.	Geben Sie alle Werte im schlüssel HKLM/Software/Firmenname aus.
Get-ItemProperty Firma:

# 12.	Lesen Sie den Wert "Zeichenkette1" aus.
(Get-ItemProperty Firma:).Zeichenkette1

# 13.	Lesen Sie den Wert "Mehrfachzeichenkette" aus.
(Get-ItemProperty Firma:).Mehrfachzeichenkette

Start-Sleep 10    # bei Skripausführung

# 14.	Ändern Sie den Wert "Zeichenkette1" auf "XYZ".
Set-ItemProperty Firma: -Name Zeichenkette1 -value "XYZ"

# 15.	Löschen Sie den Wert Mehrfachzeichenkette".
Remove-ItemProperty Firma: -Name Zeichenkette1

# 16.   Kopieren Sie den schlüssel nach Firmenname_Backup
Copy-Item hklm:\software\Firmenname hklm:\software\Firmenname_Backup

# 17.   Benennen Sie den schlüssel Firmenname_Backup um in Firmenname_Sicherung 
Rename-Item hklm:\software\Firmenname_Backup Firmenname_Sicherung

# 18.	Löschen Sie den Schlüssel HKLM/Software/Firmenname.
if (Test-Path hklm:/software/firmenname) { Remove-Item hklm:/software/firmenname }


