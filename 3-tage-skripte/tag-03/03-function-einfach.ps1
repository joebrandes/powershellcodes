﻿function Get-JBSmallFiles {
	param ( $Size = 100000 )
	Get-ChildItem E:\_temp | Where-Object {$_.Length -lt $Size -and !$_.PSIsContainer} 
}