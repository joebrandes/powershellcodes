﻿# Nur Dateien oder Ordner
Get-ChildItem c:\windows | Where-Object { $_.PSIsContainer -eq $true }     # Ordner
Get-ChildItem c:\windows | Where-Object { $_.PSIsContainer -eq $false }    # Dateien
# ab PowerShell 3.0
Get-ChildItem C:\windows -Directory
Get-ChildItem c:\windows -File
