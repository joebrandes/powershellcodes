﻿# Listing 54.22 Beispielskript für den Einsatz der allgemeinen AD-Commandlets  
# [WPS2_ADS_CommonCommandlets.ps1]
# siehe 3 Optionen

"Delete and Recreate an OU....."
# Modul PSCX: Get-ADObject
$ou = Get-ADObject -DistinguishedName "ou=Agents,dc=FBI,dc=org" 
# Modul ActiveDirectory
$ou = Get-ADObject "ou=Agents,dc=FBI,dc=org" 

$ou | Format-List

Set-ADObject "ou=Agents,dc=FBI,dc=org" -protectedFromAccidentalDeletion $false
Remove-ADObject "ou=Agents,dc=FBI,dc=org" -confirm:$false -recursive

$newou = New-ADObject -type "OrganizationalUnit" -ProtectedFromAccidentalDeletion $false -name "Alien-Agents" -Path "ou=Aliens,dc=FBI,dc=org"

"New OU:"
$newou

"Move an OU..."

Move-ADObject -Identity "ou=Alien-Agents,ou=Aliens,dc=FBI,dc=org" -targetpath "dc=FBI,dc=org"

Rename-ADObject "ou=Alien-Agents,dc=FBI,dc=org" -newname "Agents"

# Option #1
Set-ADObject "ou=Agents,dc=FBI,dc=org" -description "FBI Agents" 
# Option #2
Set-ADObject "ou=Agents,dc=FBI,dc=org" -replace @{ManagedBy="cn=Walter Skinner,ou=Directors,dc=fbi,dc=org"}
# Option #3
$newou = Get-ADObject "ou=Agents,dc=FBI,dc=org"
$newou.ManagedBy = "cn=Walter Skinner,ou=Directors,dc=fbi,dc=org"
Set-ADObject -instance $newou

"Ergebnis:"
$ou = Get-ADObject "ou=Agents,dc=FBI,dc=org"
$ou | Format-List

# ==========
# Skriptende
# ==========