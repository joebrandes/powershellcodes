﻿
<#
# Skripting (Einführung)
# ======================
#>

# Check Execution Policy
Get-ExecutionPolicy      # oder
Get-ExecutionPolicy -List 
  # als Admin für Machine oder mit -Scope CurrentUser
Set-ExecutionPolicy RemoteSigned  

# erstes Skript (nach Schwichtenberg S.96)
# Mein erstes Skript
"Informationen über diesen Computer:"
"Datum: " + (Get-Date).ToShortDateString()
"Zeit: " + (Get-Date).ToLongTimeString()
"Anzahl laufender Prozesse: " + (Get-Process).Count
"Anzahl gestarteter Dienste: " + (Get-Service | Where-Object { $_.Status -eq "running" } ).Count




# Skripte lassen sich effizient mit Alias zuweisen 
Set-Alias Get-ComputerInfos C:\powershell-bu\scripts\computerinfos.ps1






# Parameterübergaben
"Informationen über den Computer: " + $args[0]
# und per Definitionen am Skriptbeginn - so bitte später beim Skripting/Funktionen
param([string] $Computer)
"Informationen über den Computer: " + $Computer


# Mein erstes Skript - jetzt mit sauberer Definition für Parameter
param(
    [string] $Computer='mein Computer'
    )
"Informationen über den Computer: " + $Computer
"Datum: " + (Get-Date).ToShortDateString()
"Zeit: " + (Get-Date).ToLongTimeString()
"Anzahl laufender Prozesse: " + (Get-Process).Count
"Anzahl gestarteter Dienste: " + (Get-Service | Where-Object { $_.Status -eq "running" } ).Count


# Skripte pausieren lassen 
# 10 Millisekunden:
Start-Sleep -m 10
# 10 Sekunden:
Start-Sleep -s 10
