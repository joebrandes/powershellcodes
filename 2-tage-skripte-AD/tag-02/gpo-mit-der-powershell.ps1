﻿
<#
#
# Gruppenrichtlinien
# ==================
# (siehe Kapitel 55 PowerShell 5 und Core 6 - Praxisbuch)
# ------------------
# bzw. Buch Gruppenrichtlinien Voges, Dausch mit PowerShell Scripts
# in Form von Modul GroupPolicyHelper.psm1

# Nice compact Website regarding GPO Handling:
# https://cloudbrothers.info/en/manage-group-policies-powershell/
#>

Get-Command -Module GroupPolicy

Get-GPO -all
Get-GPO -Name "Name der GP"
Get-GPO -all | Where-Object { $_.displayname -ilike "d*" }  # ilike - ignore CaseSensitivity

# Default Domain Policy
Get-Gpo -Id 31b2f340-016d-11d2-945f-00c04fb984f9

# Default Domain Controllers Policy
Get-Gpo -Id 6ac1786c-016f-11d2-945f-00c04fb984f9

# List all GPOs
Get-Gpo -All

# Filter by name
Get-Gpo -All | Where-Object DisplayName -match "Default Domain"

# Backup & Restore
# ================

# Datensicherung für alle bestehenden FBI-Gruppenrichtlinien
Get-GPO -all | where { $_.displayname -like "*FBI*" } | Backup-GPO -Path –"c:\wps\GPO_backups"

# WiederherstellenRestore-GPO "GP FBI" -Path "c:\wps\GPO_backups"
Restore-GPO "GP FBI" -Path "c:\wps\GPO_backups"
Restore-GPO -all -Path "c:\wps\GPO_backups"


# Create a report

# Similar to the GUI, the configuration settings of a group policy can be exported. However, different formats can be selected.
#
#    HTML = The report known from the MMC that can be viewed in the browser.
#    XML = Output in XML format. This makes it possible e.g. to process the information with PowerShell.

Get-GPOReport -Id 31b2f340-016d-11d2-945f-00c04fb984f9 -ReportType Html -Path ~\Desktop\Report.html
Get-GPOReport -Name "GPO NAME" -ReportType Xml -Path ~\Desktop\Report.xml


# Create and Modify GPO and Links
# ===============================
# New GPO
New-GPO -Name "TESTING 01" -Comment "Joe B. with PowerShell"

# Object of GPO
$GPO = Get-Gpo -Name "TESTING 01"

# Create a new link
New-GPLink -Guid $GPO.Id -Target "OU=TestingOU,$((Get-ADDomain).DistinguishedName)" -LinkEnabled Yes -Order 1
# Change/Remove existing link
Set-GPLink -Guid $GPO.Id -Target "OU=TestingOU,$((Get-ADDomain).DistinguishedName)" -LinkEnabled No


# which GPOs are assigned to OU
Get-GPInheritance "OU=TestingOU,$((Get-ADDomain).DistinguishedName)"

# Which Permissions for OU
Get-GPPermission -Name "TESTING 01" -All



# GPO - Registry Spielerei
# ========================
# https://www.windows-faq.de/2019/05/11/per-sicherheitsrichtlinie-festlegen-nach-wie-viel-sekunden-sich-der-pc-sperren-soll/


# Windows automatisch sperren per GPO / Registry Änderung

# GPO
# Computerkonfiguration / Richtlinien / Windows-Einstellungen 
#   / Sicherheitseinstellungen / Lokale Richtlinien / Sicherheitsoptionen

# Das gleiche funktioniert auch über die Windows Registrierung. Dazu ruft Ihr im Windows Registrierungseditor „regedit.exe“ den folgenden Pfad auf.
#   HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System
# Sollte es den folgenden Wert
#   InactivityTimeoutSecs
# hier noch nicht geben, so muss er als DWORD 32-Bit Wert erstellt werden





# Listing 55.2 
# Beispiel zum Anlegen und Verlinken von Gruppenrichtlinien  
# [WPS2_GP_CreateAndLink-GPO.ps1]

Import-Module grouppolicy
# Get-GPO
# exit


# Datensicherung für alle bestehenden FBI-Gruppenrichtlinien
#Get-GPO -all | where { $_.displayname -like "*FBI*" } | Backup-GPO -Path "c:\wps\GPO_backups"
#Restore-GPO "GP FBI" -Path "c:\wps\GPO_backups"
md ad:\"ou=aliens,dc=fbi,dc=org" -ea silentlycontinue
md ad:\"ou=directors,dc=fbi,dc=org" -ea silentlycontinue
md ad:\"ou=agents,dc=fbi,dc=org" -ea silentlycontinue

# Gruppenrichtlinien löschen
Remove-GPO "GP for FBI Agents" -ea silentlycontinue
Remove-GPO "GP FBI" -ea silentlycontinue
Remove-GPO "GP for FBI Directors" -ea silentlycontinue
Remove-GPO "GP for Aliens" -ea silentlycontinue

# Neue Anlegen
new-gpo -name "GP FBI" -Comment "Standard Policy for all FBI Employees"
new-gpo -name "GP for FBI Directors" -Comment "Standard Policy for all FBI Directors"
new-gpo -name "GP for FBI Agents" -Comment "Standard Policy for all FBI Agents"
Copy-GPO -sourcename "GP FBI" -targetname "GP for Aliens"

# Verknüpfen
New-GPLink -name  "GP FBI" -target "dc=fbi,dc=org" -Linkenabled Yes
New-GPLink -name  "GP for FBI Agents" -target "ou=agents,dc=fbi,dc=org" -Linkenabled Yes
New-GPLink -name  "GP for FBI Directors" -target "ou=directors,dc=fbi,dc=org" -Linkenabled Yes
New-GPLink -name  "GP for Aliens" -target "ou=aliens,dc=fbi,dc=org" -Linkenabled Yes

# Blockieren der Vererbung
Set-GPInheritance -target "ou=agents,dc=fbi,dc=org" -isblocked no
Set-GPInheritance -target "ou=aliens,dc=fbi,dc=org" -isblocked yes


# ==========
# Skriptende
# ==========

