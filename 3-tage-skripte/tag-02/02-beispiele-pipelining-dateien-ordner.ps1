﻿# Dr. Tobias Weltner, Free PowerShell Cookbooks, www.powertheshell.com
# =======================
# filesystem_cookbook.pdf
# =======================
# Power Tips Monthly Vol.1 | June 2013


# Resolving Paths - Datei sollte existieren, sonst Fehler
Resolve-Path .\file.txt 
# Fehler unterdrücken
Resolve-Path .\file.txt -ErrorAction SilentlyContinue
# Pfad ob existent oder auch nicht!
$ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath(‘.\file.txt’)

# Nur Dateien oder Ordner
Get-ChildItem D:\_temp | Where-Object { $_.PSIsContainer -eq $true }     # Ordner
Get-ChildItem D:\_temp | Where-Object { $_.PSIsContainer -eq $false }    # Dateien
# ab PowerShell 3.0
Get-ChildItem D:\_temp -Directory
Get-ChildItem D:\_temp -File


# Desktop Ordner finden
[System.Environment]::GetFolderPath('Desktop')
# Folder anzeigen für GetFolderPath:
[System.Enum]::GetNames([System.Environment+SpecialFolder])
# also:
[System.Environment]::GetFolderPath('Cookies')


# temp Ordner aufräumen - altes Zeug 30 Tage+
# ============================================
$cutoff = (Get-Date) - (New-TimeSpan -Days 30)
$before = (Get-ChildItem $env:temp | Measure-Object Length -Sum).Sum
Get-ChildItem $env:temp | Where-Object { $_.Length -ne $null } | Where-Object { $_.LastWriteTime -lt $cutoff } |
# simulation only, no files and folders will be deleted
# replace -WhatIf with -Confirm to confirm each delete
# remove -WhatIf altogether to delete without confirmation (at your own risk)
Remove-Item -Force -ErrorAction SilentlyContinue -Recurse -WhatIf
$after = (Get-ChildItem $env:temp | Measure-Object Length -Sum).Sum
$freed = $before - $after
‘Cleanup freed {0:0.0} MB.’ -f ($freed/1MB)


# Größe eines Ordners:
# ====================
$folder = "$env:userprofile\Downloads"
Get-ChildItem -Path $folder -Recurse -Force -ea 0 | Measure-Object -Property Length -Sum | ForEach-Object { 
$sum = $_.Sum / 1MB 
"Der Download-Ordner enthält derzeit {0:#,##0.0} MB storage." -f $sum
}


# Größen Min vs. Max
# ==================
Get-ChildItem $env:windir | Measure-Object -Property Length -Minimum -Maximum | Select-Object -Property Minimum,Maximum
# Speicherzeit alt vs. neu
Get-ChildItem $env:windir |
Measure-Object -Property LastWriteTime -Minimum -Maximum |
Select-Object -Property Minimum,Maximum


# Dateien umbenennen - hier: die Win8/10 Screenshots haben Leerzeichen!
# ==================
$global:i = 0
$path = "$env:userprofile\Pictures\Screenshots"
Get-ChildItem -Path $path -Filter *.png | 
	Rename-Item -NewName { "screenshot_$i.png"; $global:i++ }

	
# 1GB Datei in Sekundenbruchteil
# ==============================
$path = ”d:\_temp\testfile.txt”
$file = [io.file]::Create($path)
$file.SetLength(1gb)
$file.Close()
Get-Item $path
	